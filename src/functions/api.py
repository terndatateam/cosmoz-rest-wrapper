from apikey import check_apikey_valid, test_apikey
from bson.objectid import ObjectId
from datetime import datetime, timezone, timedelta
from functools import partial
from functions.model_validation import validate_model
from marshmallow import Schema
from functions.mongodb import (
    MongoDBCollection,
    update,
    update_many,
    insert,
    insert_many,
    delete,
    delete_many,
)
from orjson import dumps as fast_dumps, OPT_NAIVE_UTC, OPT_UTC_Z
from sanic.exceptions import (
    Unauthorized,
    InvalidUsage,
    MethodNotSupported,
    SanicException,
)
from sanic.request import Request
from sanic.response import HTTPResponse, json
from typing import Union, Dict, List


ORJSON_OPTION = OPT_NAIVE_UTC | OPT_UTC_Z
MAX_RETURN_COUNT = 2147483647  # Highest 32bit signed int
EARLIEST_DATETIME = datetime.now(tz=timezone.utc) - timedelta(days=36525)


def get_accept_mediatypes_in_order(request: Request) -> List[str]:
    """
    Reads an Accept HTTP header and returns an array of Media Type string in descending weighted order

    :return: List of URIs of accept profiles in descending request order
    :rtype: list
    """
    try:
        # split the header into individual URIs, with weights still attached
        mtypes_list = request.headers["Accept"].split(",")
        # remove \s
        mtypes_list = [x.replace(" ", "").strip() for x in mtypes_list]

        # split off any weights and sort by them with default weight = 1
        weighted_mtypes = []
        for mtype in mtypes_list:
            mtype_parts = iter(mtype.split(";"))
            mimetype = next(mtype_parts)
            qweight = None
            try:
                while True:
                    part = next(mtype_parts)
                    if part.startswith("q="):
                        qweight = float(part.replace("q=", ""))
                        break
            except StopIteration:
                if qweight is None:
                    qweight = 1.0
            weighted_mtypes.append((qweight, mimetype))

        # sort profiles by weight, heaviest first
        weighted_mtypes.sort(reverse=True)

        return [x[1] for x in weighted_mtypes]
    except Exception as e:
        raise RuntimeError(
            "You have requested a Media Type using an Accept header that is incorrectly formatted."
        )


def match_accept_mediatypes_to_provides(request: Request, provides: List[str]):
    order = get_accept_mediatypes_in_order(request)
    for i in order:
        if i in provides:
            return i

    # try to match wildcards
    for i in order:
        if i == "*/*":
            return provides[0]
        elif i.endswith("/*"):
            check_for = i.replace("/*", "/")
            for j in provides:
                if j.startswith(check_for):
                    return j
        elif i.startswith("*/"):
            check_for = i.replace("*/", "/")
            for j in provides:
                if j.endswith(check_for):
                    return j

    return None


def get_response_type(request: Request, provides: List[str]):
    return_type = match_accept_mediatypes_to_provides(request, provides)

    format = request.args.getlist("format", None)
    if not format:
        format = request.args.getlist("_format", None)
    if format:
        format = next(iter(format))
        if format in provides:
            return_type = format

    return return_type


def make_json_error_response(message: str, code: int = 500) -> HTTPResponse:
    return HTTPResponse(
        body=fast_dumps(my_json={"result": "error", "code": code, "message": message}),
        status=code,
        content_type="application/json",
    )


def make_txt_error_response(message: str, code: int = 500) -> HTTPResponse:
    return HTTPResponse(
        body=f"Error {code}: {message}\n", status=code, content_type="text/plain"
    )


async def require_apikey(request: Request) -> Union[HTTPResponse, SanicException]:
    if request.method == "OPTIONS":
        return HTTPResponse(None, 200, None)

    existing_apikey = request.headers.getone("X-API-Key", "")

    if not existing_apikey:
        existing_apikey = request.args.get("api_key", "")

    if existing_apikey:
        return await check_api_key(request, existing_apikey)

    raise Unauthorized("Please include header X-API-Key")


async def check_api_key(
    request: Request, api_key: str
) -> Union[HTTPResponse, SanicException]:
    if request.method == "OPTIONS":
        return HTTPResponse(None, 200, None)

    if api_key is None:
        raise Unauthorized("Please include API Key in query")

    valid, message = await check_apikey_valid(api_key)
    if not valid:
        if request.method == "HEAD":
            return HTTPResponse(None, 401, None)
        else:
            if message:
                raise Unauthorized("API Key validation error: {}".format(message))
            raise Unauthorized("API Key is not valid")

    works, message = await test_apikey(api_key)
    if not works:
        if request.method == "HEAD":
            return HTTPResponse(None, 401, None)
        else:
            if message:
                raise Unauthorized("API Key Test error: {}".format(message))
            raise Unauthorized(
                "API Key is not valid but does not work. Perhaps the OAuth access key has been revoked."
            )

    return HTTPResponse(message, 200)


async def create_response(
    request: Request, content, response_type: List[str], template_map: Dict = {}
) -> Union[HTTPResponse, str]:
    if response_type == "application/json":
        return HTTPResponse(
            fast_dumps(content, option=ORJSON_OPTION),
            status=200,
            content_type=response_type,
        )

    headers = {"Content-Type": response_type}
    try:
        jinja2 = request.app.ctx.cosmoz_rest_jinja2
    except (LookupError, AttributeError):
        raise RuntimeError("Sanic-Jinja2 extension is not registered on app.")

    if response_type in template_map:
        template = template_map[response_type]
    else:
        raise NotImplementedError(
            "Cannot determine template name to use for response type."
        )

    return await jinja2.render_async(template, request, headers=headers, **content)


async def get_many_obj_response(
    request: Request,
    response_types: List[str],
    async_partial_func: partial,
    template_map: Dict = {},
) -> Union[HTTPResponse, str]:
    response_type = get_response_type(request, response_types)

    if response_type is None:
        raise InvalidUsage("Please use a valid accept type.")

    if response_type == "application/json":
        property_filter = request.args.getlist("property_filter", None)
        if property_filter:
            property_filter = str(next(iter(property_filter))).split(",")
            property_filter = [p for p in property_filter if len(p)]
    else:
        # CSV and TXT get all properties, regardless of property_filter, as it might break templates
        property_filter = "*"

    count = request.args.getlist("count", None)
    if count:
        count = min(int(next(iter(count))), MAX_RETURN_COUNT)
    else:
        count = 1000

    offset = request.args.getlist("offset", None)
    if offset:
        offset = min(int(next(iter(offset))), MAX_RETURN_COUNT)
    else:
        offset = 0

    obs_params = {
        "property_filter": property_filter,
        "count": count,
        "offset": offset,
    }

    json_safe = "orjson" if response_type == "application/json" else False
    jinja_safe = "txt" if response_type == "text/plain" else False
    jinja_safe = "csv" if response_type == "text/csv" else jinja_safe

    result = await async_partial_func(
        params=obs_params, json_safe=json_safe, jinja_safe=jinja_safe
    )

    return await create_response(request, result, response_type, template_map)


async def get_obj_response(
    request: Request,
    response_types: List[str],
    async_partial_func: partial,
    template_map: Dict = {},
) -> Union[HTTPResponse, str]:
    response_type = get_response_type(request, response_types)

    if response_type is None:
        raise MethodNotSupported("Please use a valid accept type.")

    if response_type == "application/json":
        property_filter = request.args.getlist("property_filter", None)
        if property_filter:
            property_filter = str(next(iter(property_filter))).split(",")
            property_filter = [p for p in property_filter if len(p)]
    else:
        # CSV and TXT get all properties, regardless of property_filter, as it might break templates
        property_filter = "*"

    obs_params = {
        "property_filter": property_filter,
    }

    json_safe = "orjson" if response_type == "application/json" else False
    jinja_safe = "txt" if response_type == "text/plain" else False
    jinja_safe = "csv" if response_type == "text/csv" else jinja_safe

    result = await async_partial_func(
        params=obs_params, json_safe=json_safe, jinja_safe=jinja_safe
    )

    return await create_response(request, result, response_type, template_map)


async def generic_create_request(
    collection: MongoDBCollection,
    raw_doc,
    schema_cls: Schema,
) -> HTTPResponse:
    is_valid, validation_output = validate_model(schema_cls, raw_doc, False)

    if is_valid:
        id = await insert(collection, validation_output)
        return json({"results": {"created_id": str(id)}})
    else:
        return json(validation_output, status=422)


async def generic_batch_create_request(
    collection: MongoDBCollection,
    raw_doc,
    schema_cls: Schema,
) -> HTTPResponse:
    is_valid, validation_output = validate_model(schema_cls, raw_doc, True)

    if is_valid:
        ids = await insert_many(collection, validation_output)
        return json(
            {"results": {"created": len(ids), "created_ids": [str(id) for id in ids]}}
        )
    else:
        return json(validation_output, status=422)


async def generic_update_request(
    collection: MongoDBCollection,
    selector: Union[str, ObjectId, Dict],
    raw_doc,
    schema_cls: Schema,
) -> HTTPResponse:
    is_valid, validation_output = validate_model(schema_cls, raw_doc, False)

    if is_valid:
        result = await update(collection, selector, validation_output)
        return json({"results": result})
    else:
        return json(validation_output, status=422)


async def generic_batch_update_request(
    collection: MongoDBCollection,
    raw_doc,
    schema_cls: Schema,
) -> HTTPResponse:
    is_valid, validation_output = validate_model(schema_cls, raw_doc, True)

    if is_valid:
        results = await update_many(collection, validation_output)
        return json({"results": results})
    else:
        return json(validation_output, status=422)


async def generic_delete_request(
    collection: MongoDBCollection, selector: Union[str, ObjectId, Dict]
) -> HTTPResponse:
    results = await delete(collection, selector)
    return json({"results": results})


async def generic_batch_delete_request(
    collection: MongoDBCollection, selector: Union[List[str], List[ObjectId]]
) -> HTTPResponse:
    results = await delete_many(collection, selector)
    return json({"results": results})
