import config

from ciso8601 import parse_datetime
from datetime import datetime
from influxdb_client import InfluxDBClient
from typing import Dict, Optional, Any
from urllib3.exceptions import ReadTimeoutError
from functions.utilities import datetime_to_iso


# This timeout value applies to all influxdb read requests.
INFLUX_TIMEOUT = 58 * 1000  # 58seconds, because Sanic timeout is 60 seconds
persistent_clients: Dict[str, Optional[Any]] = {"influx_client": None}


def get_influx_client():
    if persistent_clients["influx_client"] is None:
        scheme = (
            "https" if int(config.INFLUX2_PORT) in (443, 8443, 9443, 10443) else "http"
        )
        influx_client = InfluxDBClient(
            "{}://{}:{}".format(scheme, config.INFLUX2_HOST, int(config.INFLUX2_PORT)),
            config.INFLUX2_TOKEN,
            org=config.INFLUX2_ORGANIZATION,
            timeout=INFLUX_TIMEOUT,
        )
        persistent_clients["influx_client"] = influx_client

    return persistent_clients["influx_client"]


def get_last_observations_influx(site_number, params, json_safe=True, excel_safe=False):
    influx_client = get_influx_client()
    site_number = int(site_number)
    params = params or {}
    processing_level = params.get("processing_level", 3)
    property_filter = params.get("property_filter", [])
    count = params.get("count", 1)

    assert 0 <= processing_level <= 4, "Only levels 0, 1, 2, 3, 4 are acceptable."
    if processing_level < 1:
        db_measurement = "raw_values"
    else:
        db_measurement = "level{:d}".format(processing_level)

    get_all = ""
    if property_filter and len(property_filter) > 0:
        if "*" in property_filter:
            fields_string = get_all
        else:
            fields = []
            for p in property_filter:
                fields.append('r["_field"] == "{}"'.format(p))
            fields_joined = " or ".join(fields)
            fields_string = "|> filter(fn: (r) => {})".format(fields_joined)
    else:
        fields_string = get_all
    query_str = """\
    from(bucket: "cosmoz")
      |> range(start: 1970-01-01, stop: now())
      |> filter(fn: (r) => r["_measurement"] == m)
      |> filter(fn: (r) => r["site_no"] == s)
      {fields:s}
      |> tail(n: count, offset: 0)
      |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
      |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])
    """.format(
        fields=fields_string
    )
    query_api = influx_client.query_api()
    try:
        query_res = query_api.query(
            query_str,
            params={"m": db_measurement, "s": str(site_number), "count": count},
        )
    except ReadTimeoutError as e:
        print(
            f"Read timeout from InfluxDB backend when getting last-observations for site {site_number}",
            flush=True,
        )
        raise
    count = 0
    observations = {}
    for points in iter(query_res):
        for _row in points:
            this_time = _row.values.pop("_time")
            observation = _row.values
            observation["time"] = datetime_to_iso(this_time)
            if excel_safe:
                # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                observation["time"] = observation["time"].replace("T", " ")[:19]
            if "result" in observation:
                del observation["result"]
            if "table" in observation:
                del observation["table"]
            observations[this_time] = observation
            count = count + 1
    sorted_obs = [observations[d] for d in sorted(observations.keys())]
    resp = {
        "meta": {
            "site_no": site_number,
            "processing_level": processing_level,
            "count": count,
        },
        "observations": sorted_obs,
    }
    return resp


def get_daily_stats_influx(site_number, params, json_safe=True, excel_safe=False):
    influx_client = get_influx_client()
    site_number = int(site_number)
    params = params or {}
    processing_level = params.get("processing_level", 3)
    property_filter = params.get("property_filter", [])
    count = params.get("count", 2000)
    offset = params.get("offset", 0)
    startdate = params.get("startdate", None)
    enddate = params.get("enddate", None)
    if startdate is not None and isinstance(startdate, str):
        startdate = parse_datetime(startdate)
    if enddate is not None and isinstance(enddate, str):
        enddate = parse_datetime(enddate)

    assert 0 <= processing_level <= 4, "Only levels 0, 1, 2, 3 or 4 are acceptable."
    if processing_level < 1:
        db_measurement = "daily_raw_values"
    else:
        db_measurement = "daily_level{:d}".format(processing_level)

    if startdate is None:
        # set since to 1970-01-01T00:00:00Z
        start_datetime_string = "1970-01-01T00:00:00.000Z"
    else:
        if isinstance(startdate, datetime):
            start_datetime_string = startdate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(startdate, str):
            start_datetime_string = startdate
        else:
            raise RuntimeError()

    if enddate is None:
        end_datetime_string = "now()"
    else:
        if isinstance(enddate, datetime):
            end_datetime_string = enddate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(enddate, str):
            end_datetime_string = enddate
        else:
            raise RuntimeError()
    if count is not None and count > 0:
        limit_offset = "|> limit(n: c, offset: o)"
    else:
        limit_offset = ""

    get_all = ""
    if property_filter and len(property_filter) > 0:
        if "*" in property_filter:
            fields_string = get_all
        else:
            fields = []
            for p in property_filter:
                fields.append('r["_field"] == "{}"'.format(p))
            fields_joined = " or ".join(fields)
            fields_string = "|> filter(fn: (r) => {})".format(fields_joined)
    else:
        fields_string = get_all

    query_str = """\
    from(bucket: "cosmoz")
      |> range(start: {start:s}, stop: {stop:s})
      |> filter(fn: (r) => r["_measurement"] == m)
      |> filter(fn: (r) => r["site_no"] == s)
      {fields:s}
      {limit_offset:s}
      |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
      |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
        start=start_datetime_string,
        stop=end_datetime_string,
        limit_offset=limit_offset,
        fields=fields_string,
    )
    query_api = influx_client.query_api()
    try:
        query_res = query_api.query(
            query_str,
            params={
                "m": db_measurement,
                "s": str(site_number),
                "c": count,
                "o": offset,
            },
        )
    except ReadTimeoutError as e:
        print(
            f"Read timeout from InfluxDB backend when getting daily stats for site {site_number}",
            flush=True,
        )
        raise

    count = 0
    dailies = {}
    for points in iter(query_res):
        for _row in points:
            this_time = _row.values.pop("_time")
            obs_stats = _row.values
            obs_stats["time"] = datetime_to_iso(this_time)
            if excel_safe:
                # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                obs_stats["time"] = obs_stats["time"].replace("T", " ")[:19]
            if "result" in obs_stats:
                del obs_stats["result"]
            if "table" in obs_stats:
                del obs_stats["table"]
            dailies[this_time] = obs_stats
            count = count + 1
    sorted_obs = [dailies[d] for d in sorted(dailies.keys())]
    if json_safe and json_safe != "orjson":
        startdate = datetime_to_iso(startdate) if startdate else ""
        enddate = (datetime_to_iso(enddate) if enddate else "",)
    resp = {
        "meta": {
            "site_no": site_number,
            "processing_level": processing_level,
            "count": count,
            "offset": offset,
            "start_date": startdate,
            "end_date": enddate,
        },
        "daily_stats": sorted_obs,
    }
    return resp


def get_observations_influx(site_number, params, json_safe=True, excel_safe=False):
    influx_client = get_influx_client()
    site_number = int(site_number)
    params = params or {}
    processing_level = params.get("processing_level", 3)
    property_filter = params.get("property_filter", [])
    count = params.get("count", 2000)
    offset = params.get("offset", 0)
    aggregate = params.get("aggregate", None)
    if aggregate == "" or aggregate == 0:
        aggregate = None
    startdate = params.get("startdate", None)
    enddate = params.get("enddate", None)
    if startdate is not None and isinstance(startdate, str):
        startdate = parse_datetime(startdate)
    if enddate is not None and isinstance(enddate, str):
        enddate = parse_datetime(enddate)

    assert 0 <= processing_level <= 4, "Only levels 0, 1, 2, 3 or 4 are acceptable."
    if processing_level < 1:
        db_measurement = "raw_values"
    else:
        db_measurement = "level{:d}".format(processing_level)

    if startdate is None:
        # set since to 1970-01-01T00:00:00Z
        start_datetime_string = "1970-01-01T00:00:00.000Z"
    else:
        if isinstance(startdate, datetime):
            start_datetime_string = startdate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(startdate, str):
            start_datetime_string = startdate
        else:
            raise RuntimeError()

    if enddate is None:
        end_datetime_string = "now()"
    else:
        if isinstance(enddate, datetime):
            end_datetime_string = enddate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(enddate, str):
            end_datetime_string = enddate
        else:
            raise RuntimeError()
    if count is not None and count > 0:
        limit_offset = "|> limit(n: c, offset: o)"
    else:
        limit_offset = ""

    get_all = ""
    if property_filter and len(property_filter) > 0:
        if "*" in property_filter:
            fields_string = get_all
        else:
            fields = []
            for p in property_filter:
                fields.append('r["_field"] == "{}"'.format(p))
            fields_joined = " or ".join(fields)
            fields_string = "|> filter(fn: (r) => {})".format(fields_joined)
    else:
        fields_string = get_all

    if aggregate:
        # We can only aggregate over time, using values where flag == 0
        if processing_level in (0, 1, 2, 3):
            drop_flag = '|> filter(fn: (r) => r["flag"] == "0")'
        else:
            drop_flag = ""
        # Note, switch this back to aggregateWindow once the native version of min and max are merged
        # query_str = """\
        #     import "experimental"
        #     data = from(bucket: "cosmoz")
        #         |> range(start: {start:s}, stop: {stop:s})
        #         |> filter(fn: (r) => r["_measurement"] == m)
        #         |> filter(fn: (r) => r["site_no"] == s)
        #         {drop_flag:s}
        #         {fields:s}
        #         aggregate = (tables=<-, name, fn) => tables
        #             |> aggregateWindow(fn: fn, every: {ag:s}, timeSrc: "_start", createEmpty: false)
        #             |> map(fn: (r) => ({{r with _metricType: name}}))
        #             |> experimental.group(columns: ["_metricType"], mode: "extend")
        #         datamin = data |> aggregate(name: "min", fn: min)
        #         datamax = data |> aggregate(name: "max", fn: max)
        #         datamean = data |> aggregate(name: "mean", fn: mean)
        #         datacount = data |> aggregate(name: "count", fn: count)
        #
        #         union(tables: [datamin, datamax, datamean, datacount])
        #         {limit_offset:s}
        #         |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
        #         |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
        #     start=start_datetime_string, stop=end_datetime_string,
        #     limit_offset=limit_offset, fields=fields_string, ag=aggregate,
        #     drop_flag=drop_flag
        # )
        query_str = """\
        import "experimental"
        data = from(bucket: "cosmoz")
            |> range(start: {start:s}, stop: {stop:s})
            |> filter(fn: (r) => r["_measurement"] == m)
            |> filter(fn: (r) => r["site_no"] == s)
            {drop_flag:s}
            {fields:s}
        data_w = data |> window(every: {ag:s})
        data_mean = data_w
            |> experimental.mean()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "mean"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        data_min = data_w
            |> experimental.min()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "min"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        data_max = data_w
            |> experimental.max()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "max"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        data_count = data_w
            |> experimental.count()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "count"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        union(tables: [data_min, data_max, data_mean, data_count])
            {limit_offset:s}
            |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
            start=start_datetime_string,
            stop=end_datetime_string,
            limit_offset=limit_offset,
            fields=fields_string,
            ag=aggregate,
            drop_flag=drop_flag,
        )
    else:
        query_str = """\
        from(bucket: "cosmoz")
          |> range(start: {start:s}, stop: {stop:s})
          |> filter(fn: (r) => r["_measurement"] == m)
          |> filter(fn: (r) => r["site_no"] == s)
          {fields:s}
          {limit_offset:s}
          |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
          |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
            start=start_datetime_string,
            stop=end_datetime_string,
            limit_offset=limit_offset,
            fields=fields_string,
        )
    query_api = influx_client.query_api()
    try:
        query_res = query_api.query(
            query_str,
            params={
                "m": db_measurement,
                "s": str(site_number),
                "c": count,
                "o": offset,
            },
        )
    except ReadTimeoutError as e:
        print(
            f"Read timeout from InfluxDB backend when getting observations for site {site_number}",
            flush=True,
        )
        raise

    count = 0
    observations = {}
    if aggregate and query_res:
        tables = {}
        for t in iter(query_res):
            try:
                first = next(iter(t))
            except StopIteration:
                continue
            else:
                metric = first["_metricType"]
                tables[metric] = t
        try:
            mins_table = tables["min"]
            maxs_table = tables["max"]
            means_table = tables["mean"]
            counts_table = tables["count"]
        except LookupError:
            raise RuntimeError(
                "Cannot get min, max, mean, count responses from InfluxDB"
            )
        for max_row, min_row, mean_row, count_row in zip(
            maxs_table, mins_table, means_table, counts_table
        ):
            this_time = mean_row.values.pop("_time")
            observation = {"time": datetime_to_iso(this_time)}
            if excel_safe:
                # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                observation["time"] = observation["time"].replace("T", " ")[:19]
            for pfx, row_vals in (
                ("max_", max_row.values),
                ("min_", min_row.values),
                ("mean_", mean_row.values),
                ("count_", count_row.values),
            ):
                for k, v in row_vals.items():
                    if k in (
                        "_time",
                        "time",
                        "result",
                        "table",
                        "_metricType",
                        "site_no",
                        "flag",
                    ):
                        continue
                    observation[pfx + str(k)] = v
            observations[this_time] = observation
            count = count + 1
    else:
        # Not aggregate query
        for points in iter(query_res):
            for _row in points:
                this_time = _row.values.pop("_time")
                observation = _row.values
                observation["time"] = datetime_to_iso(this_time)
                if excel_safe:
                    # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                    observation["time"] = observation["time"].replace("T", " ")[:19]
                if "result" in observation:
                    del observation["result"]
                if "table" in observation:
                    del observation["table"]
                observations[this_time] = observation
                count = count + 1
    sorted_obs = [observations[d] for d in sorted(observations.keys())]
    if json_safe and json_safe != "orjson":
        startdate = datetime_to_iso(startdate) if startdate else ""
        enddate = (datetime_to_iso(enddate) if enddate else "",)
    resp = {
        "meta": {
            "site_no": site_number,
            "processing_level": processing_level,
            "count": count,
            "offset": offset,
            "start_date": startdate,
            "end_date": enddate,
        },
        "observations": sorted_obs,
    }
    if aggregate:
        resp["meta"]["aggregation"] = str(aggregate)
    return resp


def get_statuses_by_station(site_number, params, json_safe=True, excel_safe=False):
    """
    :param site_number:
    :param params:
    :param json_safe:
    :param excel_safe:
    :return:
    """
    influx_client = get_influx_client()
    site_number = int(site_number)
    params = params or {}
    property_filter = params.get("property_filter", [])
    count = params.get("count", 2000)
    offset = params.get("offset", 0)
    startdate = params.get("startdate", None)
    enddate = params.get("enddate", None)
    if startdate is not None and isinstance(startdate, str):
        startdate = parse_datetime(startdate)
    if enddate is not None and isinstance(enddate, str):
        enddate = parse_datetime(enddate)

    if startdate is None:
        # set since to 1970-01-01T00:00:00Z
        start_datetime_string = "1970-01-01T00:00:00.000Z"
    else:
        if isinstance(startdate, datetime):
            start_datetime_string = startdate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(startdate, str):
            start_datetime_string = startdate
        else:
            raise RuntimeError()

    if enddate is None:
        end_datetime_string = "now()"
    else:
        if isinstance(enddate, datetime):
            end_datetime_string = enddate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(enddate, str):
            end_datetime_string = enddate
        else:
            raise RuntimeError()
    if count is not None and count > 0:
        limit_offset = "|> limit(n: c, offset: o)"
    else:
        limit_offset = ""

    get_all = ""
    if property_filter and len(property_filter) > 0:
        if "*" in property_filter:
            fields_string = get_all
        else:
            fields = []
            for p in property_filter:
                fields.append('r["_field"] == "{}"'.format(p))
            fields_joined = " or ".join(fields)
            fields_string = "|> filter(fn: (r) => {})".format(fields_joined)
    else:
        fields_string = get_all

    query_str = """\
    from(bucket: "cosmoz")
      |> range(start: {start:s}, stop: {stop:s})
      |> filter(fn: (r) => r["_measurement"] == "status_data")
      |> filter(fn: (r) => r["site_no"] == s)
      {fields:s}
      {limit_offset:s}
      |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
      |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
        start=start_datetime_string,
        stop=end_datetime_string,
        limit_offset=limit_offset,
        fields=fields_string,
    )
    query_api = influx_client.query_api()
    try:
        query_res = query_api.query(
            query_str, params={"s": str(site_number), "c": count, "o": offset}
        )
    except ReadTimeoutError as e:
        print(
            f"Read timeout from InfluxDB backend when getting status records for site {site_number}",
            flush=True,
        )
        raise
    count = 0
    status_records = {}

    for points in iter(query_res):
        for _row in points:
            this_time = _row.values.pop("_time")
            status_row = _row.values
            status_row["time"] = datetime_to_iso(this_time)
            _ = status_row.pop("remote_password")
            _ = status_row.pop("satellite_password_error")
            if "result" in status_row:
                del status_row["result"]
            if "table" in status_row:
                del status_row["table"]
            if excel_safe:
                # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                status_row["time"] = status_row["time"].replace("T", " ")[:19]
            status_records[this_time] = status_row
            count = count + 1
    sorted_records = [status_records[d] for d in sorted(status_records.keys())]
    resp = {
        "meta": {
            "site_no": site_number,
            "count": count,
            "offset": offset,
            "start_date": startdate,
            "end_date": enddate,
        },
        "status": sorted_records,
    }
    return resp


def get_ref_intensities(site_number, params, json_safe=True, excel_safe=False):
    """
    property_filter can have "time", "intensity", "bad_data_flag"
    :param site_number:
    :param params:
    :param json_safe:
    :param excel_safe:
    :return:
    """
    influx_client = get_influx_client()
    site_number = int(site_number)
    params = params or {}
    property_filter = params.get("property_filter", [])
    count = params.get("count", 2000)
    offset = params.get("offset", 0)
    aggregate = params.get("aggregate", None)
    if aggregate == "" or aggregate == 0:
        aggregate = None
    startdate = params.get("startdate", None)
    enddate = params.get("enddate", None)
    if startdate is not None and isinstance(startdate, str):
        startdate = parse_datetime(startdate)
    if enddate is not None and isinstance(enddate, str):
        enddate = parse_datetime(enddate)

    if startdate is None:
        # set since to 1970-01-01T00:00:00Z
        start_datetime_string = "1970-01-01T00:00:00.000Z"
    else:
        if isinstance(startdate, datetime):
            start_datetime_string = startdate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(startdate, str):
            start_datetime_string = startdate
        else:
            raise RuntimeError()

    if enddate is None:
        end_datetime_string = "now()"
    else:
        if isinstance(enddate, datetime):
            end_datetime_string = enddate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(enddate, str):
            end_datetime_string = enddate
        else:
            raise RuntimeError()
    if count is not None and count > 0:
        limit_offset = "|> limit(n: c, offset: o)"
    else:
        limit_offset = ""

    get_all = ""
    if property_filter and len(property_filter) > 0:
        if "*" in property_filter:
            fields_string = get_all
        else:
            fields = []
            for p in property_filter:
                fields.append('r["_field"] == "{}"'.format(p))
            fields_joined = " or ".join(fields)
            fields_string = "|> filter(fn: (r) => {})".format(fields_joined)
    else:
        fields_string = get_all

    if aggregate:
        # Note, switch this back to aggregateWindow once the native version of min and max are merged
        # query_str = """\
        #     import "experimental"
        #     data = from(bucket: "cosmoz")
        #         |> range(start: {start:s}, stop: {stop:s})
        #         |> filter(fn: (r) => r["_measurement"] == "intensity")
        #         |> filter(fn: (r) => r["bad_data_flag"] == "0")
        #         |> filter(fn: (r) => r["site_no"] == s)
        #         {fields:s}
        #         aggregate = (tables=<-, name, fn) => tables
        #             |> aggregateWindow(fn: fn, every: {ag:s}, timeSrc: "_start", createEmpty: false)
        #             |> map(fn: (r) => ({{r with _metricType: name}}))
        #             |> experimental.group(columns: ["_metricType"], mode: "extend")
        #         datamin = data |> aggregate(name: "min", fn: min)
        #         datamax = data |> aggregate(name: "max", fn: max)
        #         datamean = data |> aggregate(name: "mean", fn: mean)
        #         datacount = data |> aggregate(name: "count", fn: count)
        #
        #         union(tables: [datamin, datamax, datamean, datacount])
        #         {limit_offset:s}
        #         |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
        #         |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no", "bad_data_flag"])""".format(
        #     start=start_datetime_string, stop=end_datetime_string,
        #     limit_offset=limit_offset, fields=fields_string, ag=aggregate
        # )
        query_str = """\
        import "experimental"
        data = from(bucket: "cosmoz")
            |> range(start: {start:s}, stop: {stop:s})
            |> filter(fn: (r) => r["_measurement"] == "intensity")
            |> filter(fn: (r) => r["bad_data_flag"] == "0")
            |> filter(fn: (r) => r["site_no"] == s)
            {fields:s}
        data_w = data |> window(every: {ag:s})
        data_mean = data_w
            |> experimental.mean()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "mean"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        data_min = data_w
            |> experimental.min()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "min"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        data_max = data_w
            |> experimental.max()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "max"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        data_count = data_w
            |> experimental.count()
            |> duplicate(column: "_start", as: "_time")
            |> map(fn: (r) => ({{r with _metricType: "count"}}))
            |> experimental.group(columns: ["_metricType"], mode: "extend")
        union(tables: [data_min, data_max, data_mean, data_count])
            {limit_offset:s}
            |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
            start=start_datetime_string,
            stop=end_datetime_string,
            limit_offset=limit_offset,
            fields=fields_string,
            ag=aggregate,
        )
    else:
        query_str = """\
        from(bucket: "cosmoz")
          |> range(start: {start:s}, stop: {stop:s})
          |> filter(fn: (r) => r["_measurement"] == "intensity")
          |> filter(fn: (r) => r["site_no"] == s)
          {fields:s}
          {limit_offset:s}
          |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
          |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
            start=start_datetime_string,
            stop=end_datetime_string,
            limit_offset=limit_offset,
            fields=fields_string,
        )
    query_api = influx_client.query_api()
    try:
        query_res = query_api.query(
            query_str, params={"s": str(site_number), "c": count, "o": offset}
        )
    except ReadTimeoutError as e:
        print(
            f"Read timeout from InfluxDB backend when getting intensities for site {site_number}",
            flush=True,
        )
        raise
    count = 0
    intensities = {}
    if aggregate and query_res:
        tables = {}
        for t in iter(query_res):
            try:
                first = next(iter(t))
            except StopIteration:
                continue
            else:
                metric = first["_metricType"]
                tables[metric] = t
        try:
            mins_table = tables["min"]
            maxs_table = tables["max"]
            means_table = tables["mean"]
            counts_table = tables["count"]
        except LookupError:
            raise RuntimeError(
                "Cannot get min, max, mean, count responses from InfluxDB"
            )
        for max_row, min_row, mean_row, count_row in zip(
            maxs_table, mins_table, means_table, counts_table
        ):
            this_time = mean_row.values.pop("_time")
            intens = {"time": datetime_to_iso(this_time)}
            if excel_safe:
                # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                intens["time"] = intens["time"].replace("T", " ")[:19]
            for pfx, row_vals in (
                ("max_", max_row.values),
                ("min_", min_row.values),
                ("mean_", mean_row.values),
                ("count_", count_row.values),
            ):
                for k, v in row_vals.items():
                    if k in (
                        "_time",
                        "time",
                        "result",
                        "flag",
                        "table",
                        "_metricType",
                        "site_no",
                        "bad_data_flag",
                    ):
                        continue
                    intens[pfx + str(k)] = v
            intensities[this_time] = intens
            count = count + 1
    else:
        for points in iter(query_res):
            for _row in points:
                this_time = _row.values.pop("_time")
                intens = _row.values
                intens["time"] = datetime_to_iso(this_time)
                if "result" in intens:
                    del intens["result"]
                if "table" in intens:
                    del intens["table"]
                if excel_safe:
                    # hack to very quickly convert iso 8601 to yyyy-MM-dd hh:mm:ss for excel
                    intens["time"] = intens["time"].replace("T", " ")[:19]
                intensities[this_time] = intens
                count = count + 1
    sorted_intens = [intensities[d] for d in sorted(intensities.keys())]
    resp = {
        "meta": {
            "site_no": site_number,
            "count": count,
            "offset": offset,
            "start_date": startdate,
            "end_date": enddate,
        },
        "intensities": sorted_intens,
    }
    if aggregate:
        resp["meta"]["aggregation"] = str(aggregate)
    return resp
