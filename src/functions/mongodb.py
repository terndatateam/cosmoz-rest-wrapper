import asyncio
import config

from bson.decimal128 import Decimal128
from datetime import datetime
from enum import Enum
from typing import Dict, Optional, Any, List, Union, Tuple
from motor.motor_asyncio import (
    AsyncIOMotorClient as MotorClient,
    AsyncIOMotorGridFSBucket,
)
from bson.objectid import ObjectId
from pathlib import Path
from collections import OrderedDict
from functions.utilities import datetime_to_iso
from pymongo import UpdateOne, DeleteOne
from pymongo.cursor import Cursor


class MongoDBCollection(Enum):
    STATION_COLLECTION = "all_stations"
    CALIBRATION_COLLECTION = "stations_calibration_v2"


persistent_clients: Dict[str, Optional[Any]] = {"mongo_client": None}


def get_mongo_client():
    if persistent_clients["mongo_client"] is None:
        persistent_clients["mongo_client"] = MotorClient(
            config.MONGODB_HOST, config.MONGODB_PORT, io_loop=asyncio.get_event_loop()
        )

    return persistent_clients["mongo_client"]


def props_to_projection(prop_list, required_list=[]):
    select_filter = OrderedDict()

    if prop_list and len(prop_list) > 0 and "*" not in prop_list:
        select_filter = OrderedDict({v: True for v in required_list})
        select_filter.update(OrderedDict({v: True for v in prop_list}))

    # explicitly remove the id if not mentioned
    if not (prop_list and "_id" in prop_list) and "_id" not in required_list:
        select_filter["_id"] = False

    if "_id" in select_filter:
        select_filter.move_to_end("_id", last=False)

    if len(select_filter) == 0:
        return None

    return select_filter


def clean_record(record: Dict, json_safe: bool = True, jinja_safe: bool = True) -> None:
    if jinja_safe and "status" in record:
        record["_status"] = record["status"]
        del record["status"]

    for r, v in record.items():
        if isinstance(v, datetime):
            if (
                json_safe and json_safe != "orjson"
            ) or jinja_safe:  # orjson can handle native datetimes
                v = datetime_to_iso(v)
            record[r] = v
        elif isinstance(v, Decimal128):
            g = v.to_decimal()
            if json_safe and g.is_nan():
                g = "NaN"
            elif json_safe == "orjson":  # orjson can't do decimal
                g = float(
                    g
                )  # converting to float is fine because Javascript numbers are native double-float anyway.
            record[r] = g


async def get_record(
    collection: MongoDBCollection, query: Dict, projection: Union[List, Dict]
) -> Tuple[int, Cursor]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]
    session = await client.start_session()

    try:
        count = await coll.count_documents({})
        row = await coll.find_one(query, projection=projection, session=session)
    finally:
        await session.end_session()

    if row is None or len(row) < 1:
        raise LookupError("Cannot find record.")

    return count, row


async def get_records(
    collection: MongoDBCollection, query: Dict, projection: Union[List, Dict]
) -> Tuple[int, List]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]
    session = await client.start_session()

    try:
        total = await coll.count_documents(query)
        cursor = coll.find(query, projection=projection, session=session)
        all_results = await cursor.to_list(length=None)
    finally:
        await session.end_session()

    return total, all_results


async def get_station_mongo(
    station_number: int,
    params: Dict = {},
    json_safe: bool = True,
    jinja_safe: bool = False,
) -> Dict:
    station_number = int(station_number)
    property_filter = params.get("property_filter", [])
    select_filter = props_to_projection(property_filter, ["site_no"])

    total, record = await get_record(
        MongoDBCollection.STATION_COLLECTION, {"site_no": station_number}, select_filter
    )
    clean_record(record, json_safe, jinja_safe)

    # make the response id the site_no so ember has an 'id'
    record["id"] = record["site_no"]
    return {
        "meta": {"total": total},
        "station": record,
    }


async def get_station_calibration_mongo(
    station_number: int, params: Dict, json_safe: bool = True, jinja_safe: bool = False
) -> Dict:
    # count = params.get('count', 1000)
    # offset = params.get('offset', 0)
    property_filter = params.get("property_filter", [])
    select_filter = props_to_projection(property_filter, ["_id", "site_no"])

    total, records = await get_records(
        MongoDBCollection.CALIBRATION_COLLECTION,
        {"site_number": station_number},
        select_filter,
    )

    for record in records:
        clean_record(record, json_safe, jinja_safe)
        record["id"] = str(record["_id"])
        del record["_id"]

    count = len(records)
    return {
        "meta": {
            "total": total,
            "count": count,
            "offset": 0,  # not implemented yet
        },
        "calibrations": records,
    }


async def get_stations_mongo(
    params: Dict, json_safe: bool = True, jinja_safe: bool = False
) -> Dict:
    # count = params.get('count', 1000)
    # offset = params.get('offset', 0)

    property_filter = params.get("property_filter", [])
    select_filter = props_to_projection(property_filter, ["_id", "site_no"])

    total, records = await get_records(
        MongoDBCollection.STATION_COLLECTION, {}, select_filter
    )

    for record in records:
        clean_record(record, json_safe, jinja_safe)
        record["id"] = str(record["site_no"])
        del record["_id"]

    count = len(records)
    return {
        "meta": {
            "total": total,
            "count": count,
            "offset": 0,  # not implemented yet
        },
        "stations": records,
    }


async def get_calibration_mongo(
    calibration_id: str,
    params: Dict = {},
    json_safe: bool = True,
    jinja_safe: bool = False,
) -> Dict:
    property_filter = params.get("property_filter", [])
    select_filter = props_to_projection(property_filter, ["_id"])

    total, record = await get_record(
        MongoDBCollection.CALIBRATION_COLLECTION,
        {"_id": ObjectId(calibration_id)},
        select_filter,
    )
    clean_record(record, json_safe, jinja_safe)

    record["id"] = str(record["_id"])
    del record["_id"]

    return {
        "meta": {"total": total},
        "calibration": record,
    }


async def is_unique_val(
    collection: MongoDBCollection, field_name: str, val: Any
) -> bool:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    query = {}
    query[field_name] = val

    result = await coll.find_one(query)

    return not result


async def get_existing_values(
    collection: MongoDBCollection, field_name: str, vals: List[Any]
) -> List[Any]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    query = {}
    query[field_name] = {"$in": vals}

    existing_values = []
    async for doc in coll.find(query):
        existing_values.append(doc[field_name])

    return existing_values


async def insert_file_stream(filename: str, stream: Any) -> ObjectId:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    gfs = AsyncIOMotorGridFSBucket(db)

    file_id = await gfs.upload_from_stream(
        filename,
        stream,
        # metadata={"contentType": "text/plain"}
    )

    return file_id


async def find_db_file(filename: str) -> Any | None:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    gfs = AsyncIOMotorGridFSBucket(db)

    cursor = gfs.find({"filename": filename})

    async for file_obj in cursor:
        return gfs.wrap(file_obj)

    return None


async def write_file_to_stream(filename: str, stream_resp) -> None:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    gfs = AsyncIOMotorGridFSBucket(db)

    grid_out = await gfs.open_download_stream_by_name(filename)

    while True:
        chunk = await grid_out.readchunk()
        if not chunk:
            break
        await stream_resp.send(chunk)

    await stream_resp.send(b"", end_stream=True)


async def load_default_images() -> None:
    file_dir = Path("./static/images")

    for p in file_dir.iterdir():
        path = p.resolve()
        print(f"Adding the image '{path}' to the database")

        if not path.name.startswith("."):
            with open(path, mode="rb") as f:
                await insert_file_stream(p.name, f)


async def insert(collection: MongoDBCollection, doc: Dict) -> Any:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    result = await coll.insert_one(doc)

    return result.inserted_id


async def insert_many(collection: MongoDBCollection, docs: List[Dict]) -> List[Any]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    result = await coll.insert_many(docs)

    return result.inserted_ids


async def update(
    collection: MongoDBCollection,
    filter_criteria: Union[str, ObjectId, Dict],
    doc: Dict,
) -> Dict[str, int]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    if not isinstance(filter_criteria, dict):
        if isinstance(filter_criteria, ObjectId):
            filter_criteria = {"_id": filter_criteria}
        else:
            filter_criteria = {"_id": ObjectId(filter_criteria)}

    result = await coll.update_one(filter_criteria, {"$set": doc})

    return {"matched": result.matched_count, "modified": result.modified_count}


async def update_many(
    collection: MongoDBCollection, docs: List[Dict]
) -> Dict[str, int]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    for doc in docs:
        if "id" not in doc:
            raise Exception(
                'Batch updates require all items in payload to include an "id" field.'
            )
        else:
            if isinstance(doc["id"], ObjectId):
                doc["_id"] = doc["id"]
            else:
                doc["_id"] = ObjectId(doc["id"])
            doc.pop("id")

    bulk_operations = []
    for doc in docs:
        doc_id = doc["_id"]
        filter_criteria = {"_id": doc_id}
        update_operation = {"$set": doc}

        bulk_operations.append(
            UpdateOne(filter_criteria, update_operation, upsert=True)
        )

    result = await coll.bulk_write(bulk_operations)

    return {"matched": result.matched_count, "modified": result.modified_count}


async def delete(
    collection: MongoDBCollection, filter_criteria: Union[str, ObjectId, Dict]
) -> Dict[str, int]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    if not isinstance(filter_criteria, dict):
        if isinstance(filter_criteria, ObjectId):
            filter_criteria = {"_id": filter_criteria}
        else:
            filter_criteria = {"_id": ObjectId(filter_criteria)}

    result = await coll.delete_one(filter_criteria)

    return {"deleted": result.deleted_count}


async def delete_many(
    collection: MongoDBCollection, ids: List[ObjectId]
) -> Dict[str, int]:
    client = get_mongo_client()
    db = client[config.MONGODB_NAME]
    coll = db[collection.value]

    bulk_operations = []
    for id in ids:
        filter_criteria = {"_id": id}

        bulk_operations.append(DeleteOne(filter_criteria))

    result = await coll.bulk_write(bulk_operations)

    return {"deleted": result.deleted_count}
