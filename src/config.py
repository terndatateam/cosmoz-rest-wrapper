# -*- coding: utf-8 -*-
#
import sys
from functions.utilities import load_env
from os import getenv
import pathlib

load_env()
module = sys.modules[__name__]
TRUTHS = {True, 1, "1", "T", "t", "true", "TRUE", "True"}
CONFIG = module.CONFIG = {}
default_image_path = str(
    pathlib.Path(pathlib.Path(__file__).parent.absolute(), "static/images")
)

OVERRIDE_SERVER_NAME = CONFIG["OVERRIDE_SERVER_NAME"] = getenv(
    "SANIC_OVERRIDE_SERVER_NAME", "localhost:8080"
)
PROXY_ROUTE_BASE = CONFIG["PROXY_ROUTE_BASE"] = getenv(
    "SANIC_PROXY_ROUTE_BASE", ""
).rstrip("/")
SANIC_SERVER_NAME = CONFIG["SANIC_SERVER_NAME"] = getenv("SANIC_SERVER_NAME", "")
# INFLUXDB_HOST = CONFIG['INFLUXDB_HOST'] = getenv("INFLUX_DB_HOST", "cosmoz.influxdb")
# INFLUXDB_PORT = CONFIG['INFLUXDB_PORT'] = int(getenv("INFLUX_DB_PORT", 8086))
# INFLUXDB_USERNAME = CONFIG['INFLUXDB_USERNAME'] = getenv("INFLUX_DB_USERNAME", None)
# INFLUXDB_PASSWORD = CONFIG['INFLUXDB_PASSWORD'] = getenv("INFLUX_DB_PASSWORD", None)
# INFLUXDB_NAME = CONFIG['INFLUXDB_NAME'] = getenv("INFLUX_DB_NAME", "cosmoz")
INFLUX2_HOST = CONFIG["INFLUX2_HOST"] = getenv("INFLUX2_HOST", "influxdb")
INFLUX2_PORT = CONFIG["INFLUX2_PORT"] = int(getenv("INFLUX2_PORT", 8086))
INFLUX2_BUCKET = CONFIG["INFLUX2_BUCKET"] = getenv("INFLUX2_BUCKET", "cosmoz")
INFLUX2_ORGANIZATION = CONFIG["INFLUX2_ORGANIZATION"] = getenv(
    "INFLUX2_ORGANIZATION", "tern-landscapes"
)
INFLUX2_USERNAME = CONFIG["INFLUX2_USERNAME"] = getenv("INFLUX2_USERNAME", "username")
INFLUX2_PASSWORD = CONFIG["INFLUX2_PASSWORD"] = getenv("INFLUX2_PASSWORD", "password")
INFLUX2_TOKEN = CONFIG["INFLUX2_TOKEN"] = getenv("INFLUX2_TOKEN", "xxtoken==")

MONGODB_HOST = CONFIG["MONGODB_HOST"] = getenv("MONGO_DB_HOST", "cosmoz.mongodb")
MONGODB_PORT = CONFIG["MONGODB_PORT"] = int(getenv("MONGO_DB_PORT", 27017))
MONGODB_NAME = CONFIG["MONGODB_NAME"] = getenv("MONGO_DB_NAME", "cosmoz")
METRICS_DIRECTORY = CONFIG["METRICS_DIRECTORY"] = getenv("METRICS_DIRECTORY", ".")
DEBUG = CONFIG["DEBUG"] = getenv("SANIC_DEBUG", "") in TRUTHS
AUTO_RELOAD = CONFIG["AUTO_RELOAD"] = getenv("SANIC_AUTO_RELOAD", "") in TRUTHS
# UPLOAD_DIR = CONFIG['UPLOAD_DIR'] = getenv("UPLOAD_DIR", default_image_path)
