# -*- coding: utf-8 -*-
"""
Copyright 2019 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import datetime

from functions.api import (
    MAX_RETURN_COUNT,
    EARLIEST_DATETIME,
    ORJSON_OPTION,
    match_accept_mediatypes_to_provides,
    make_json_error_response,
    require_apikey,
    get_many_obj_response,
    get_obj_response,
    generic_update_request,
    generic_create_request,
)
from collections import OrderedDict
from config import TRUTHS
from datetime import datetime, timezone, timedelta
from functools import partial
from functions.influxdb import (
    get_daily_stats_influx,
    get_observations_influx,
    get_last_observations_influx,
    get_statuses_by_station,
    get_ref_intensities,
)
from functions.model_validation import format_errors
from marshmallow import ValidationError
from models import (
    StationSchema,
)
from functions.mongodb import (
    MongoDBCollection,
    is_unique_val,
    get_station_mongo,
    get_stations_mongo,
)
from orjson import dumps as fast_dumps
from sanic_restplus import Resource, Namespace
from sanic.response import json, text, HTTPResponse
from urllib3.exceptions import ReadTimeoutError
from functions.utilities import luhn_checksum_mod10

ns = Namespace(
    name="Stations",
    description="Endpoints for accessing and updating CosmOz station data.",
    path="/stations",
)


@ns.route("")
class Stations(Resource):
    accept_types = ["application/json", "text/csv", "text/plain"]
    """Gets a JSON representation of all sites in the COSMOZ database."""

    @ns.doc(
        "get_stations",
        params=OrderedDict(
            [
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "count",
                    {
                        "description": "Number of records to return.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 1000,
                    },
                ),
                (
                    "offset",
                    {
                        "description": "Skip number of records before reading count.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 0,
                    },
                ),
            ]
        ),
        security=None,
    )
    @ns.produces(["application/json"])
    async def get(self, request, *args, **kwargs):
        """Get cosmoz stations."""
        return await get_many_obj_response(
            request, self.accept_types, get_stations_mongo
        )

    @ns.doc("post_station", security={"APIKeyQueryParam": [], "APIKeyHeader": []})
    async def post(self, request, *args, **kwargs):
        """Add new cosmoz station."""
        await require_apikey(request)

        if not request.json or "station" not in request.json:
            return text("station must be in the payload", status=400)

        station = request.json["station"]
        if "site_no" in station and not await is_unique_val(
            MongoDBCollection.STATION_COLLECTION, "site_no", int(station["site_no"])
        ):
            err = ValidationError({"site_no": ["Value already in use"]})
            payload = format_errors(StationSchema(), err.messages, False)
            return json(payload, status=422)

        return await generic_create_request(
            MongoDBCollection.STATION_COLLECTION,
            station,
            StationSchema,
        )


@ns.route("/<station_no>")
@ns.param("station_no", "Station Number", type="number", format="integer")
@ns.response(404, "Station not found")
class Station(Resource):
    accept_types = ["application/json", "text/csv", "text/plain"]
    """Gets site date for station_no."""

    @ns.doc(
        "get_station",
        params=OrderedDict(
            [
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, station_no=None, **kwargs):
        """Get cosmoz station."""
        if station_no is None:
            raise RuntimeError("station_no is mandatory.")

        station_no = int(station_no)
        get_station_func = partial(get_station_mongo, station_no)

        return await get_obj_response(
            request,
            self.accept_types,
            get_station_func,
            {"text/plain": "site_values_txt.html"},
        )

    @ns.doc("put_station", security={"APIKeyQueryParam": [], "APIKeyHeader": []})
    @ns.produces(accept_types)
    async def put(self, request, *args, station_no=None, **kwargs):
        """Update cosmoz station calibration"""
        await require_apikey(request)

        if not request.json or "station" not in request.json:
            return text("'station' must be in the body payload", status=400)

        return await generic_update_request(
            MongoDBCollection.STATION_COLLECTION,
            {"site_no": int(station_no)},
            request.json["station"],
            StationSchema,
        )


@ns.route("/<station_no>/daily_stats")
@ns.param("station_no", "Station Number", type="number", format="integer")
class DailyStats(Resource):
    accept_types = ["application/json", "text/csv"]
    """Gets aggregated 24H min, max, mean, and count, fast lookup of pre-computed daily stats."""

    @ns.doc(
        "get_records",
        params=OrderedDict(
            [
                (
                    "processing_level",
                    {
                        "description": "Query the table for this processing level.\n\n"
                        "(0, 1, 2, 3, or 4).",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 4,
                    },
                ),
                (
                    "startdate",
                    {
                        "description": "Start of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-06-01T00:00:00Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "enddate",
                    {
                        "description": "End of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-07-01T23:59:59Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "excel_compat",
                    {
                        "description": "Use MS Excel compatible datetime column in CSV and TXT responses.",
                        "required": False,
                        "type": "boolean",
                        "default": False,
                    },
                ),
                (
                    "count",
                    {
                        "description": "Number of records to return.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": MAX_RETURN_COUNT,
                    },
                ),
                (
                    "offset",
                    {
                        "description": "Skip number of records before reading count.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 0,
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, station_no=None, **kwargs):
        """Get cosmoz daily aggregated 24h stats records."""
        nowtime = datetime.utcnow().replace(tzinfo=timezone.utc)
        return_type = match_accept_mediatypes_to_provides(request, self.accept_types)
        format = request.args.getlist("format", None)
        if not format:
            format = request.args.getlist("_format", None)
        if format:
            format = next(iter(format))
            if format in self.accept_types:
                return_type = format
        if return_type is None:
            return_type = "application/json"
        if station_no is None:
            raise RuntimeError("station_no is mandatory.")
        station_no = int(station_no)
        processing_level = request.args.getlist("processing_level", None)
        if processing_level:
            processing_level = int(next(iter(processing_level)))
        else:
            processing_level = 4
        not_json = return_type != "application/json"
        if not not_json:
            property_filter = request.args.getlist("property_filter", None)
            if property_filter:
                property_filter = str(next(iter(property_filter))).split(",")
                property_filter = [p for p in property_filter if len(p)]
        else:
            property_filter = "*"
        excel_compat = request.args.getlist("excel_compat", [False])[0] in TRUTHS
        startdate = request.args.getlist("startdate", None)
        if startdate:
            startdate = next(iter(startdate))
        else:
            if not not_json:
                startdate = (nowtime + timedelta(days=-365)).replace(
                    hour=0, minute=0, second=0, microsecond=0
                )
            else:
                startdate = EARLIEST_DATETIME
        enddate = request.args.getlist("enddate", None)
        if enddate:
            enddate = next(iter(enddate))
        else:
            enddate = nowtime.replace(hour=23, minute=59, second=59, microsecond=0)
        count = request.args.getlist("count", None)
        if not not_json:
            fallback_count = 5000
        else:
            fallback_count = MAX_RETURN_COUNT
        if count:
            try:
                count = min(int(next(iter(count))), MAX_RETURN_COUNT)
            except ValueError:
                count = fallback_count
        else:
            count = MAX_RETURN_COUNT
        offset = request.args.getlist("offset", None)
        fallback_offset = 0
        if offset:
            try:
                offset = min(int(next(iter(offset))), MAX_RETURN_COUNT)
            except ValueError:
                offset = fallback_offset
        else:
            offset = fallback_offset
        obs_params = {
            "processing_level": processing_level,
            "property_filter": property_filter,
            "startdate": startdate,
            "enddate": enddate,
            "count": count,
            "offset": offset,
        }
        if not not_json:
            json_safe = "orjson"
            try:
                try:
                    res = get_daily_stats_influx(
                        station_no, obs_params, json_safe, False
                    )
                except ReadTimeoutError:
                    return make_json_error_response("Database Read timeout", code=500)
                resp = HTTPResponse(
                    fast_dumps(res, option=ORJSON_OPTION),
                    status=200,
                    content_type=return_type,
                )
                return resp
            except Exception as e:
                print(e)
                raise e
        headers = {"Content-Type": return_type}
        try:
            jinja2 = request.app.ctx.cosmoz_rest_jinja2
        except (LookupError, AttributeError):
            raise RuntimeError("Sanic-Jinja2 extension is not registered on app.")
        if return_type == "text/csv":
            if processing_level == 0:
                template = "raw_daily_csv.html"
            else:
                template = "level{}_daily_csv.html".format(processing_level)
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_daily_level{}.csv"'.format(
                    str(station_no), str(processing_level)
                )
            )
        else:
            raise RuntimeError("Invalid Return Type")
        resp = await request.respond(
            status=200, headers=headers, content_type=return_type
        )
        if count > 1000:
            # send the headers first
            _ = await resp.send(b"", end_stream=False)
        try:
            res = get_daily_stats_influx(station_no, obs_params, False, excel_compat)
        except ReadTimeoutError:
            await resp.send(f"Error 500: Database Read timeout\n")
            return None
        r = await jinja2.render_string_async(template, request, **res)
        await resp.send(r)
        return None


@ns.route("/<station_no>/observations")
@ns.param("station_no", "Station Number", type="number", format="integer")
class Observations(Resource):
    accept_types = ["application/json", "text/csv", "text/plain"]
    """Gets a JSON representation of observation records in the COSMOZ database."""

    @ns.doc(
        "get_records",
        params=OrderedDict(
            [
                (
                    "processing_level",
                    {
                        "description": "Query the table for this processing level.\n\n"
                        "(0, 1, 2, 3, or 4).",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 4,
                    },
                ),
                (
                    "startdate",
                    {
                        "description": "Start of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-06-01T00:00:00Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "enddate",
                    {
                        "description": "End of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-07-01T23:59:59Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "aggregate",
                    {
                        "description": "Average observations over a given time period.\n\n"
                        "Eg. `2h` or `3m` or `1d`",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "excel_compat",
                    {
                        "description": "Use MS Excel compatible datetime column in CSV and TXT responses.",
                        "required": False,
                        "type": "boolean",
                        "default": False,
                    },
                ),
                (
                    "count",
                    {
                        "description": "Number of records to return.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": MAX_RETURN_COUNT,
                    },
                ),
                (
                    "offset",
                    {
                        "description": "Skip number of records before reading count.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 0,
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, station_no=None, **kwargs):
        """Get cosmoz records."""
        nowtime = datetime.utcnow().replace(tzinfo=timezone.utc)
        return_type = match_accept_mediatypes_to_provides(request, self.accept_types)
        format = request.args.getlist("format", None)
        if not format:
            format = request.args.getlist("_format", None)
        if format:
            format = next(iter(format))
            if format in self.accept_types:
                return_type = format
        if return_type is None:
            return_type = "application/json"
        if station_no is None:
            raise RuntimeError("station_no is mandatory.")
        station_no = int(station_no)
        processing_level = request.args.getlist("processing_level", None)
        if processing_level:
            processing_level = int(next(iter(processing_level)))
        else:
            processing_level = 4
        not_json = return_type != "application/json"
        if not not_json:
            property_filter = request.args.getlist("property_filter", None)
            if property_filter:
                property_filter = str(next(iter(property_filter))).split(",")
                property_filter = [p for p in property_filter if len(p)]
        else:
            property_filter = "*"
        excel_compat = request.args.getlist("excel_compat", [False])[0] in TRUTHS
        aggregate = request.args.getlist("aggregate", None)
        if aggregate:
            aggregate = str(next(iter(aggregate)))
        startdate = request.args.getlist("startdate", None)
        if startdate:
            startdate = next(iter(startdate))
        else:
            if not not_json:
                startdate = (nowtime + timedelta(days=-365)).replace(
                    hour=0, minute=0, second=0, microsecond=0
                )
            else:
                startdate = EARLIEST_DATETIME
        enddate = request.args.getlist("enddate", None)
        if enddate:
            enddate = next(iter(enddate))
        else:
            enddate = nowtime.replace(hour=23, minute=59, second=59, microsecond=0)
        count = request.args.getlist("count", None)
        if not not_json:
            fallback_count = 5000
        else:
            fallback_count = MAX_RETURN_COUNT
        if count:
            try:
                count = min(int(next(iter(count))), MAX_RETURN_COUNT)
            except ValueError:
                count = fallback_count
        else:
            count = MAX_RETURN_COUNT
        offset = request.args.getlist("offset", None)
        fallback_offset = 0
        if offset:
            try:
                offset = min(int(next(iter(offset))), MAX_RETURN_COUNT)
            except ValueError:
                offset = fallback_offset
        else:
            offset = fallback_offset
        obs_params = {
            "processing_level": processing_level,
            "property_filter": property_filter,
            "aggregate": aggregate,
            "startdate": startdate,
            "enddate": enddate,
            "count": count,
            "offset": offset,
        }
        if not not_json:
            json_safe = "orjson"
            try:
                try:
                    res = get_observations_influx(
                        station_no, obs_params, json_safe, False
                    )
                except ReadTimeoutError:
                    return make_json_error_response("Database Read timeout", code=500)
                resp = HTTPResponse(
                    fast_dumps(res, option=ORJSON_OPTION),
                    status=200,
                    content_type=return_type,
                )
                return resp
            except Exception as e:
                print(e)
                raise e
        headers = {"Content-Type": return_type}
        try:
            jinja2 = request.app.ctx.cosmoz_rest_jinja2
        except (LookupError, AttributeError):
            raise RuntimeError("Sanic-Jinja2 extension is not registered on app.")
        if return_type == "text/csv":
            if processing_level == 0:
                template = "raw_data_csv.html"
            else:
                template = "level{}_data_csv.html".format(processing_level)
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_level{}.csv"'.format(
                    str(station_no), str(processing_level)
                )
            )
        elif return_type == "text/plain":
            headers = {"Content-Type": return_type}
            if processing_level == 0:
                template = "raw_data_txt.html"
            else:
                template = "level{}_data_txt.html".format(processing_level)
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_level{}.txt"'.format(
                    str(station_no), str(processing_level)
                )
            )
        else:
            raise RuntimeError("Invalid Return Type")
        resp = await request.respond(
            status=200, headers=headers, content_type=return_type
        )
        if count > 1000:
            # send the headers first
            _ = await resp.send(b"", end_stream=False)
        try:
            res = get_observations_influx(station_no, obs_params, False, excel_compat)
        except ReadTimeoutError:
            await resp.send(f"Error 500: Database Read timeout\n")
            return None
        # Check if response uses rain or rain_mm, and adjust the template accordingly
        if (
            "observations" in res
            and len(res["observations"]) > 0
            and "rain_mm" in res["observations"][0]
        ):
            template = str(template).replace("_data_", "_data_mm_")
        r = await jinja2.render_string_async(template, request, **res)
        await resp.send(r)
        return None


@ns.route("/<station_no>/lastobservations")
@ns.param("station_no", "Station Number", type="number", format="integer")
class LastObservations(Resource):
    """Gets a JSON representation of recent observation records in the COSMOZ database."""

    accept_types = ["application/json", "text/csv", "text/plain"]

    @ns.doc(
        "get_records",
        params=OrderedDict(
            [
                (
                    "processing_level",
                    {
                        "description": "Query the table for this processing level.\n\n"
                        "(0, 1, 2, 3, or 4).",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 4,
                    },
                ),
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "excel_compat",
                    {
                        "description": "Use MS Excel compatible datetime column in CSV and TXT responses.",
                        "required": False,
                        "type": "boolean",
                        "default": False,
                    },
                ),
                (
                    "count",
                    {
                        "description": "Number of records to return.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 1,
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, station_no=None, **kwargs):
        """Get recent cosmoz records."""
        return_type = match_accept_mediatypes_to_provides(request, self.accept_types)
        format = request.args.getlist("format", None)
        if not format:
            format = request.args.getlist("_format", None)
        if format:
            format = next(iter(format))
            if format in self.accept_types:
                return_type = format
        if return_type is None:
            return_type = "application/json"
        if station_no is None:
            raise RuntimeError("station_no is mandatory.")
        station_no = int(station_no)
        processing_level = request.args.getlist("processing_level", None)
        if processing_level:
            processing_level = int(next(iter(processing_level)))
        else:
            processing_level = 4
        excel_compat = request.args.getlist("excel_compat", [False])[0] in TRUTHS
        not_json = return_type != "application/json"
        if not not_json:
            property_filter = request.args.getlist("property_filter", None)
            if property_filter:
                property_filter = str(next(iter(property_filter))).split(",")
                property_filter = [p for p in property_filter if len(p)]
        else:
            property_filter = "*"
        count = request.args.getlist("count", None)
        if count:
            try:
                count = min(int(next(iter(count))), MAX_RETURN_COUNT)
            except ValueError:
                count = 1
        else:
            count = 1
        obs_params = {
            "processing_level": processing_level,
            "property_filter": property_filter,
            "count": count,
        }
        if not not_json:
            json_safe = "orjson"
            try:
                try:
                    res = get_last_observations_influx(
                        station_no, obs_params, json_safe, False
                    )
                except ReadTimeoutError:
                    return make_json_error_response("Database Read timeout", code=500)
                resp = HTTPResponse(
                    fast_dumps(res, option=ORJSON_OPTION),
                    status=200,
                    content_type=return_type,
                )
                return resp
            except ReadTimeoutError:
                return make_json_error_response("Database Read timeout", code=500)
            except Exception as e:
                print(e)
                raise e
        headers = {"Content-Type": return_type}
        try:
            jinja2 = request.app.ctx.cosmoz_rest_jinja2
        except (LookupError, AttributeError):
            raise RuntimeError("Sanic-Jinja2 extension is not registered on app.")
        if return_type == "text/csv":
            if processing_level == 0:
                template = "raw_data_csv.html"
            else:
                template = "level{}_data_csv.html".format(processing_level)
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_level{}.csv"'.format(
                    str(station_no), str(processing_level)
                )
            )
        elif return_type == "text/plain":
            headers = {"Content-Type": return_type}
            if processing_level == 0:
                template = "raw_data_txt.html"
            else:
                template = "level{}_data_txt.html".format(processing_level)
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_level{}.txt"'.format(
                    str(station_no), str(processing_level)
                )
            )
        else:
            raise RuntimeError("Invalid Return Type")
        resp = await request.respond(
            status=200, headers=headers, content_type=return_type
        )
        if count > 1000:
            # send the headers first
            _ = await resp.send(b"", end_stream=False)
        try:
            res = get_last_observations_influx(
                station_no, obs_params, False, excel_compat
            )
        except ReadTimeoutError:
            await resp.send(f"Error 500: Database Read timeout\n")
            return None
        # Check if response uses rain or rain_mm, and adjust the template accordingly
        if (
            "observations" in res
            and len(res["observations"]) > 0
            and "rain_mm" in res["observations"][0]
        ):
            template = str(template).replace("_data_", "_data_mm_")
        r = await jinja2.render_string_async(template, request, **res)
        await resp.send(r)
        return None


@ns.route("/<station_no>/status")
@ns.param("station_no", "Station Number", type="number", format="integer")
class Statuses(Resource):
    accept_types = ["application/json", "text/csv"]
    """Gets the status records from the CRNS Instruments."""

    @ns.doc(
        "get_records",
        params=OrderedDict(
            [
                (
                    "startdate",
                    {
                        "description": "Start of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-06-01T00:00:00Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "enddate",
                    {
                        "description": "End of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-07-01T23:59:59Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "excel_compat",
                    {
                        "description": "Use MS Excel compatible datetime column in CSV and TXT responses.",
                        "required": False,
                        "type": "boolean",
                        "default": False,
                    },
                ),
                (
                    "count",
                    {
                        "description": "Number of records to return.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": MAX_RETURN_COUNT,
                    },
                ),
                (
                    "offset",
                    {
                        "description": "Skip number of records before reading count.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 0,
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, station_no=None, **kwargs):
        """Get status records."""
        nowtime = datetime.utcnow().replace(tzinfo=timezone.utc)
        return_type = match_accept_mediatypes_to_provides(request, self.accept_types)
        format = request.args.getlist("format", None)
        if not format:
            format = request.args.getlist("_format", None)
        if format:
            format = next(iter(format))
            if format in self.accept_types:
                return_type = format
        if return_type is None:
            return_type = "application/json"
        if station_no is None:
            raise RuntimeError("station_no is mandatory.")
        station_is_imei = len(station_no) == 15
        if station_is_imei:
            v = luhn_checksum_mod10(station_no)
            if not v == 0:
                raise RuntimeError("invalid IMEI.")
        else:
            station_no = int(station_no)
        not_json = return_type != "application/json"
        if not not_json:
            property_filter = request.args.getlist("property_filter", None)
            if property_filter:
                property_filter = str(next(iter(property_filter))).split(",")
                property_filter = [p for p in property_filter if len(p)]
        else:
            # Property Filter doesn't work in CSV mode, because it is a fixed format
            property_filter = "*"
        # Excel compat treats dates differently when serializing to CSV
        excel_compat = request.args.getlist("excel_compat", [False])[0] in TRUTHS
        startdate = request.args.getlist("startdate", None)
        if startdate:
            startdate = next(iter(startdate))
        else:
            if not not_json:
                startdate = (nowtime + timedelta(days=-365)).replace(
                    hour=0, minute=0, second=0, microsecond=0
                )
            else:
                startdate = EARLIEST_DATETIME
        enddate = request.args.getlist("enddate", None)
        if enddate:
            enddate = next(iter(enddate))
        else:
            enddate = nowtime.replace(hour=23, minute=59, second=59, microsecond=0)
        count = request.args.getlist("count", None)
        if not not_json:
            fallback_count = 5000
        else:
            fallback_count = MAX_RETURN_COUNT
        if count:
            try:
                count = min(int(next(iter(count))), MAX_RETURN_COUNT)
            except ValueError:
                count = fallback_count
        else:
            count = MAX_RETURN_COUNT
        offset = request.args.getlist("offset", None)
        fallback_offset = 0
        if offset:
            try:
                offset = min(int(next(iter(offset))), MAX_RETURN_COUNT)
            except ValueError:
                offset = fallback_offset
        else:
            offset = fallback_offset
        # TODO: Implement status_by_imei, this needs a new raw imei-to-status table, probably in MongoDB
        obs_params = {
            "property_filter": property_filter,
            "startdate": startdate,
            "enddate": enddate,
            "count": count,
            "offset": offset,
        }
        if not not_json:
            json_safe = "orjson"
            try:
                try:
                    res = get_statuses_by_station(
                        station_no, obs_params, json_safe, False
                    )
                except ReadTimeoutError:
                    return make_json_error_response("Database Read timeout", code=500)
                resp = HTTPResponse(
                    fast_dumps(res, option=ORJSON_OPTION),
                    status=200,
                    content_type=return_type,
                )
                return resp
            except Exception as e:
                print(e)
                raise e
        headers = {"Content-Type": return_type}
        try:
            jinja2 = request.app.ctx.cosmoz_rest_jinja2
        except (LookupError, AttributeError):
            raise RuntimeError("Sanic-Jinja2 extension is not registered on app.")
        if return_type == "text/csv":
            template = "statuses_csv.html"
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_status.csv"'.format(str(station_no))
            )
        else:
            raise RuntimeError("Invalid Return Type")
        resp = await request.respond(
            status=200, headers=headers, content_type=return_type
        )
        if count > 1000:
            # send the headers first
            _ = await resp.send(b"", end_stream=False)
        try:
            res = get_statuses_by_station(station_no, obs_params, False, excel_compat)
        except ReadTimeoutError:
            await resp.send(f"Error 500: Database Read timeout\n")
            return None
        r = await jinja2.render_string_async(template, request, **res)
        await resp.send(r)
        return None


@ns.route("/<station_no>/intensities")
@ns.param("station_no", "Station Number", type="number", format="integer")
class Intensities(Resource):
    accept_types = ["application/json", "text/csv", "text/plain"]
    """Gets a JSON representation of NMDB Intensity records in the COSMOZ database."""

    @ns.doc(
        "get_intensity_records",
        params=OrderedDict(
            [
                (
                    "startdate",
                    {
                        "description": "Start of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-06-01T00:00:00Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "enddate",
                    {
                        "description": "End of the date/time range, in ISO8601 format.\n\n"
                        "_Eg: `2017-07-01T23:59:59Z`_\n\n",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "aggregate",
                    {
                        "description": "Average observations over a given time period.\n\n"
                        "Eg. `2h` or `3m` or `1d`",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
                (
                    "excel_compat",
                    {
                        "description": "Use MS Excel compatible datetime column in CSV and TXT responses.",
                        "required": False,
                        "type": "boolean",
                        "default": False,
                    },
                ),
                (
                    "count",
                    {
                        "description": "Number of records to return.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": MAX_RETURN_COUNT,
                    },
                ),
                (
                    "offset",
                    {
                        "description": "Skip number of records before reading count.",
                        "required": False,
                        "type": "number",
                        "format": "integer",
                        "default": 0,
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, station_no=None, **kwargs):
        """Get cosmoz intensity refs."""
        nowtime = datetime.utcnow().replace(tzinfo=timezone.utc)
        return_type = match_accept_mediatypes_to_provides(request, self.accept_types)
        format = request.args.getlist("format", None)
        if not format:
            format = request.args.getlist("_format", None)
        if format:
            format = next(iter(format))
            if format in self.accept_types:
                return_type = format
        if return_type is None:
            return_type = "application/json"
        if station_no is None:
            raise RuntimeError("station_no is mandatory.")
        station_no = int(station_no)
        not_json = return_type != "application/json"
        if not not_json:
            property_filter = request.args.getlist("property_filter", None)
            if property_filter:
                property_filter = str(next(iter(property_filter))).split(",")
                property_filter = [p for p in property_filter if len(p)]
        else:
            property_filter = ["*"]
        excel_compat = request.args.getlist("excel_compat", [False])[0] in TRUTHS
        aggregate = request.args.getlist("aggregate", None)
        if aggregate:
            aggregate = str(next(iter(aggregate)))
        startdate = request.args.getlist("startdate", None)
        if startdate:
            startdate = next(iter(startdate))
        else:
            if not not_json:
                startdate = (nowtime + timedelta(days=-365)).replace(
                    hour=0, minute=0, second=0, microsecond=0
                )
            else:
                startdate = EARLIEST_DATETIME
        enddate = request.args.getlist("enddate", None)
        if enddate:
            enddate = next(iter(enddate))
        else:
            enddate = nowtime.replace(hour=23, minute=59, second=59, microsecond=0)
        count = request.args.getlist("count", None)
        if not not_json:
            fallback_count = 5000
        else:
            fallback_count = MAX_RETURN_COUNT
        if count:
            try:
                count = min(int(next(iter(count))), MAX_RETURN_COUNT)
            except ValueError:
                count = fallback_count
        else:
            count = MAX_RETURN_COUNT
        offset = request.args.getlist("offset", None)
        fallback_offset = 0
        if offset:
            try:
                offset = min(int(next(iter(offset))), MAX_RETURN_COUNT)
            except ValueError:
                offset = fallback_offset
        else:
            offset = fallback_offset
        obs_params = {
            "property_filter": property_filter,
            "aggregate": aggregate,
            "startdate": startdate,
            "enddate": enddate,
            "count": count,
            "offset": offset,
        }
        if not not_json:
            json_safe = "orjson"
            try:
                try:
                    res = get_ref_intensities(station_no, obs_params, json_safe, False)
                except ReadTimeoutError:
                    return make_json_error_response("Database Read timeout", code=500)
                resp = HTTPResponse(
                    fast_dumps(res, option=ORJSON_OPTION),
                    status=200,
                    content_type=return_type,
                )
                return resp
            except Exception as e:
                print(e)
                raise e
        headers = {"Content-Type": return_type}
        try:
            jinja2 = request.app.ctx.cosmoz_rest_jinja2
        except (LookupError, AttributeError):
            raise RuntimeError("Sanic-Jinja2 extension is not registered on app.")
        if return_type == "text/csv":
            template = "intensities_csv.html"
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_intensities.csv"'.format(
                    str(station_no)
                )
            )
        elif return_type == "text/plain":
            template = "intensities_txt.html"
            headers["Content-Disposition"] = (
                'attachment; filename="station{}_intensities.txt"'.format(
                    str(station_no)
                )
            )
        else:
            raise RuntimeError("Invalid Return Type")

        resp = await request.respond(
            status=200, headers=headers, content_type=return_type
        )
        if count > 1000:
            # send the headers first
            _ = await resp.send(b"", end_stream=False)
        try:
            res = get_ref_intensities(station_no, obs_params, False, excel_compat)
        except ReadTimeoutError:
            await resp.send(f"Error 500: Database Read timeout\n")
            return None
        r = await jinja2.render_string_async(template, request, **res)
        await resp.send(r)
        return None
