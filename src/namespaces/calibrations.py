# -*- coding: utf-8 -*-
"""
Copyright 2019 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from functions.api import (
    require_apikey,
    get_obj_response,
    generic_update_request,
    generic_delete_request,
    generic_batch_update_request,
    generic_batch_create_request,
    generic_batch_delete_request,
)
from bson.objectid import ObjectId
from collections import OrderedDict
from functools import partial
from models import (
    CalibrationSchema,
)
from functions.mongodb import (
    MongoDBCollection,
    get_existing_values,
    get_station_calibration_mongo,
    get_calibration_mongo,
)
from sanic_restplus import Resource, Namespace
from sanic.request import Request
from sanic.response import json, text

ns = Namespace(
    name="Calibrations",
    description="Endpoints for accessing and updating CosmOz station calibration records.",
    path="/calibrations",
)


@ns.route("")
@ns.param("station_no", "Station Number", type="number", format="integer", _in="query")
@ns.response(404, "Calibrations not found")
class Calibrations(Resource):
    accept_types = ["application/json", "text/csv", "text/plain"]
    """Gets site date for station_no."""

    @ns.doc(
        "get_station_cal",
        params=OrderedDict(
            [
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request: Request, *args, **kwargs):
        """Get cosmoz station calibrations."""
        station_no = request.args.get("station_no", None)

        if station_no is None:
            return text("station_no is mandatory.", status=400)

        try:
            station_no = int(station_no)
        except:
            return text("station_no value must be a valid integer.", status=400)

        get_cals_func = partial(get_station_calibration_mongo, station_no)

        return await get_obj_response(
            request,
            self.accept_types,
            get_cals_func,
            {
                "text/csv": "site_data_cal_csv_v2.html",
                "text/plain": "site_data_cal_txt_v2.html",
            },
        )

    @ns.doc("post_calibrations", security={"APIKeyQueryParam": [], "APIKeyHeader": []})
    async def post(self, request: Request, *args, **kwargs):
        """Add new cosmoz station calibration."""
        await require_apikey(request)

        if not request.json or "calibrations" not in request.json:
            return text("calibrations must be in the payload", status=400)

        calibrations = request.json["calibrations"]
        if len(calibrations) == 0:
            return text("calibrations cannot be empty", status=400)

        return await generic_batch_create_request(
            MongoDBCollection.CALIBRATION_COLLECTION,
            calibrations,
            CalibrationSchema,
        )

    @ns.doc("put_calibrations", security={"APIKeyQueryParam": [], "APIKeyHeader": []})
    async def put(self, request: Request, *args, **kwargs):
        """Batch update cosmoz station calibrations"""
        await require_apikey(request)

        if not request.json or "calibrations" not in request.json:
            return text("calibration must be in the payload", status=400)

        calibrations = request.json["calibrations"]
        if len(calibrations) == 0:
            return text("calibrations cannot be empty", status=400)

        calibration_ids = []
        for calibration in calibrations:
            if "id" not in calibration:
                return text(
                    "All calibration records to be updated must have an id field present.",
                    status=400,
                )

            calibration_ids.append(ObjectId(calibration["id"]))

        # Request should not proceed if request contains more than one record with the same id.
        if len(set(calibration_ids)) != len(calibration_ids):
            duplicate_ids = [
                cal_id
                for cal_id in set(calibration_ids)
                if calibration_ids.count(cal_id) > 1
            ]

            return json(
                {
                    "message": "Request cannot be processed as two or more calibration records in the request contains the same id.",
                    "ids": [str(id) for id in duplicate_ids],
                },
                status=400,
            )

        # Request should not proceed if one or more calibration records provided references a non-existent id.
        existing_ids = await get_existing_values(
            MongoDBCollection.CALIBRATION_COLLECTION, "_id", calibration_ids
        )
        nonexistent_ids = set(calibration_ids) - set(existing_ids)

        if len(nonexistent_ids) != 0:
            return json(
                {
                    "message": "Request cannot be processed as one or more calibration records references a non existent id.",
                    "ids": [str(id) for id in list(nonexistent_ids)],
                },
                status=400,
            )

        return await generic_batch_update_request(
            MongoDBCollection.CALIBRATION_COLLECTION,
            calibrations,
            CalibrationSchema,
        )

    @ns.doc("delete_calibrations", security={"APIQueryParam": [], "APIKeyHeader": []})
    async def delete(self, request: Request, *args, **kwargs):
        await require_apikey(request)

        if not request.json or "calibration_ids" not in request.json:
            return text("calibration_ids must be in the payload", status=400)

        calibration_ids = request.json["calibration_ids"]
        if len(calibration_ids) == 0:
            return text("calibration_ids cannot be empty", status=400)

        cal_ids = []
        for cal_id in calibration_ids:
            cal_ids.append(ObjectId(cal_id))

        return await generic_batch_delete_request(
            MongoDBCollection.CALIBRATION_COLLECTION, cal_ids
        )


@ns.route("/<c_id>")
@ns.param("c_id", "Calibration ID", type="string")
@ns.response(404, "Calibration not found")
class Calibration(Resource):
    accept_types = ["application/json", "text/csv", "text/plain"]

    @ns.doc(
        "get_calibration",
        params=OrderedDict(
            [
                (
                    "property_filter",
                    {
                        "description": "Comma delimited list of properties to retrieve.\n\n"
                        "_Enter * for all_.",
                        "required": False,
                        "type": "string",
                        "format": "text",
                    },
                ),
            ]
        ),
    )
    @ns.produces(accept_types)
    async def get(self, request, *args, c_id=None, **kwargs):
        """Get cosmoz station calibrations."""
        if c_id is None:
            return text("id is mandatory.", status=400)

        get_calibration_func = partial(get_calibration_mongo, c_id)

        return await get_obj_response(
            request,
            self.accept_types,
            get_calibration_func,
            {
                "text/csv": "site_data_cal_csv_single_v2.html",
                "text/plain": "site_data_cal_txt_single_v2.html",
            },
        )

    @ns.doc("put_calibration", security={"APIKeyQueryParam": [], "APIKeyHeader": []})
    async def put(self, request, *args, c_id=None, **kwargs):
        """Update cosmoz station calibration"""
        await require_apikey(request)

        if c_id is None:
            return text("id is mandatory", status=400)

        if not request.json or "calibration" not in request.json:
            text("calibration must be in the payload", status=400)

        return await generic_update_request(
            MongoDBCollection.CALIBRATION_COLLECTION,
            c_id,
            request.json["calibration"],
            CalibrationSchema,
        )

    @ns.doc("delete_calibration", security={"APIKeyQueryParam": [], "APIKeyHeader": []})
    async def delete(self, request, *args, c_id=None, **kwargs):
        """Delete cosmoz station calibration"""
        await require_apikey(request)

        if c_id is None:
            return text("id is mandatory", status=400)

        return await generic_delete_request(
            MongoDBCollection.CALIBRATION_COLLECTION, c_id
        )
