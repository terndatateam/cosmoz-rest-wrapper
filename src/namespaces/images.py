# -*- coding: utf-8 -*-
"""
Copyright 2019 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import aiofiles
import config

from functions.api import (
    require_apikey,
)
from math import ceil
from functions.mongodb import (
    find_db_file,
    insert_file_stream,
)
from sanic_restplus import Resource, Namespace
from sanic.response import json
from sanic.exceptions import ServerError


ns = Namespace(
    name="Images",
    description="Endpoints for uploading and retrieving CosmOz station images.",
    path="/images",
)


@ns.route("")
class ImageUpload(Resource):
    write_locally = False

    async def post(self, request):
        await require_apikey(request)

        # Ensure a file was sent
        upload_file = request.files.get("file")
        if not upload_file:
            return json({"error": "Missing file"}, status=400)

        if self.write_locally:
            file_path = f"{config.UPLOAD_DIR}/{upload_file.name}"
            await self.write_file(file_path, upload_file.body)
        else:
            file_id = await insert_file_stream(upload_file.name, upload_file.body)

        return json({"result": "ok"})

    @classmethod
    async def write_file(cls, path, body):
        async with aiofiles.open(path, "wb") as f:
            await f.write(body)


@ns.route("/<filename>")
@ns.param("filename", "Image Filename", type="string", format="string")
class ImageDownload(Resource):
    async def get(self, request, *args, filename=None):
        if filename is None:
            return json({"error": "Missing file name"}, status=404)
        file_obj = await find_db_file(filename)
        if not file_obj:
            return json({"error": "File not found"}, status=404)
        ftype = (
            "image/jpeg"
            if (filename.endswith(".jpeg") or filename.endswith(".jpg"))
            else "image/png"
        )
        print(
            f"serving image {file_obj.filename} with length {file_obj.length}",
            flush=True,
        )
        resp = await request.respond(
            status=200, headers={"content-length": file_obj.length}, content_type=ftype
        )
        await resp.send(b"", False)  # send just the headers first
        async for chunk in file_obj:
            chunk_len = len(chunk)
            if chunk is None or chunk_len < 1:
                break
            if resp.stream is None or resp.stream.send is None:
                return None
            bytes_left = getattr(resp.stream, "response_bytes_left", None)
            if bytes_left is not None and (chunk_len > bytes_left):
                return None
            subchunks = ceil(chunk_len / 16384.0)
            print(f"sending chunk with length {chunk_len}", flush=True)
            try:
                for s in range(subchunks):
                    subchunk_start = s * 16384
                    subchunk_end = min(chunk_len, subchunk_start + 16384)
                    if (subchunk_end - subchunk_start) < 1:
                        break
                    await resp.send(chunk[subchunk_start:subchunk_end])
            except ServerError:
                # stream is gone, or bytes exceeds content-length?
                break
        if resp.stream is not None and resp.stream.response_func is not None:
            try:
                await resp.eof()
            except ServerError:
                resp.stream.response_func = None
