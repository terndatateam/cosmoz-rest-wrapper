# -*- coding: utf-8 -*-
"""
Copyright 2019 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from functions.auth import token_auth
from ciso8601 import parse_datetime
from orjson import dumps as fast_dumps
from sanic_restplus import Api, Resource
from sanic.response import HTTPResponse
from urllib.parse import urlsplit

from namespaces.calibrations import ns as calibrations_namespace
from namespaces.stations import ns as stations_namespace
from namespaces.images import ns as images_namespace


security_defs = {
    # X-API-Key: abcdef12345
    "APIKeyHeader": {"type": "apiKey", "in": "header", "name": "X-API-Key"},
    "APIKeyQueryParam": {"type": "apiKey", "in": "query", "name": "api_key"},
    "AuthToken": {"type": "apiKey", "in": "header", "name": "Authorization"},
}

api = Api(
    title="CSIRO CosmOz REST Interface",
    prefix="",
    doc="/doc",
    authorizations=security_defs,
    default_mediatype="application/json",
    additional_css="/static/material_swagger.css",
)

ns = api.default_namespace
ns.description = "Endpoints for various CosmOz network administration tools"

api.add_namespace(calibrations_namespace)
api.add_namespace(stations_namespace)
api.add_namespace(images_namespace)


@ns.route("/metrics", doc=False)
class Metrics(Resource):
    async def post(self, request, context):
        rcontext = context.for_request(request)
        shared_context = context.shared
        shared_rcontext = shared_context.for_request(request)

        action = request.args.getlist("action", None)
        if action:
            action = next(iter(action))
        else:
            raise RuntimeError("action is mandatory.")
        metrics_override = {
            "method": "GET",
            "status": 200,
            "skip_response": True,
        }
        referer = request.headers.getall("Referer")
        if referer:
            referer = next(iter(referer))
            try:
                (scheme, netloc, path, query, fragment) = urlsplit(referer)
                metrics_override["host"] = netloc
                metrics_override["path"] = path
            except:
                pass
        else:
            # No referer, this will be hard to track
            pass
        if action == "page_visit":
            time = request.args.getlist("time", None)
            if time:
                time = next(iter(time))
                try:
                    metrics_override["datetime_start_iso"] = time
                    t = parse_datetime(time)
                    metrics_override["timestamp_start"] = t.timestamp()
                    metrics_override["datetime_start"] = t
                except Exception:
                    pass
            page = request.args.getlist("page", None)
            if page:
                page = next(iter(page))
                metrics_override["path"] = page
            query = request.args.getlist("query", None)
            if query:
                query = next(iter(query))
            else:
                query = None
            metrics_override["qs"] = query
        else:
            raise NotImplementedError(action)
        shared_rcontext["override_metrics"] = metrics_override
        res = {"result": "success"}
        return HTTPResponse(
            fast_dumps(res), status=200, content_type="application/json"
        )


@ns.route("/users/me")
class Users(Resource):
    @ns.doc("get_userfortoken", security=["AuthToken"])
    @token_auth.login_required
    async def get(self, request):
        """Exchange a valid auth token for the user details, perhaps an API_KEY if needed"""
        user = token_auth.current_user(request)
        if user:
            return {
                "user": user,
            }
