# -*- coding: utf-8 -*-
"""
Copyright 2019 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import base64
from inspect import isawaitable
from os import getenv

import oauthlib.oauth2
from sanic.response import redirect, text
from sanic_plugin_toolkit import SanicPluginRealm
from sanic_plugin_toolkit.plugins.contextualize import contextualize
from sanic_oauthlib.client import oauthclient, parse_response
from sanic_session_sptk import session as session_plugin
from filesystem_session_interface import FilesystemSessionInterface
from functions.utilities import load_env


# having these in a module-local _hopefully_ shouldn't be a problem
# using them async might be an issue, but maybe not
OAUTH2_REMOTES = {}


def add_oauth_plugin(app):
    realm = SanicPluginRealm(app)
    try:
        oauth = realm.register_plugin(oauthclient)
    except ValueError as v:
        _, oauth = v.args
    return oauth


def create_csiro_ldap_oauth2_remote(app, oauth=None):
    if not oauth:
        oauth = add_oauth_plugin(app)
    consumer_key = getenv("OAUTH2_CSIRO_LDAP_CONSUMER_KEY", "example1")
    consumer_secret = getenv("OAUTH2_CSIRO_LDAP_CONSUMER_SECRET", "password1")
    oauth_base_uri = getenv("OAUTH2_CSIRO_LDAP_BASE_URI", "https://oauth.esoil.io/")
    remote = oauth.remote_app(
        "csiro-to-ldap2",
        consumer_key=consumer_key,
        consumer_secret=consumer_secret,
        request_token_params={"scope": "profile"},
        base_url=f"{oauth_base_uri}api/",
        access_token_method="POST",
        access_token_url=f"{oauth_base_uri}oauth2/token",
        authorize_url=f"{oauth_base_uri}oauth2/authorize",
    )
    OAUTH2_REMOTES["csiro-to-ldap2"] = remote
    return remote


def create_landscapes_keycloak_oauth2_remote(app, oauth=None):
    if not oauth:
        oauth = add_oauth_plugin(app)
    consumer_key = getenv("OAUTH2_LANDSCAPES_KEYCLOAK_CONSUMER_KEY", "example1")
    consumer_secret = getenv("OAUTH2_LANDSCAPES_KEYCLOAK_CONSUMER_SECRET", "password1")
    oauth_base_uri = getenv(
        "OAUTH2_LANDSCAPES_KEYCLOAK_BASE_URI", "https://auth.tern.org.au/auth/"
    )
    keycloak_realm = getenv("OAUTH2_LANDSCAPES_KEYCLOAK_REALM", "csiro-landscapes")
    remote = oauth.remote_app(
        "landscapes-keycloak2",
        consumer_key=consumer_key,
        consumer_secret=consumer_secret,
        request_token_params={"scope": "profile"},
        base_url=f"{oauth_base_uri}",
        access_token_method="POST",
        access_token_url=f"{oauth_base_uri}realms/{keycloak_realm}/protocol/openid-connect/token",
        authorize_url=f"{oauth_base_uri}realms/{keycloak_realm}/protocol/openid-connect/auth",
    )
    setattr(
        remote,
        "introspect_url",
        f"{oauth_base_uri}realms/{keycloak_realm}/protocol/openid-connect/token/introspect",
    )
    OAUTH2_REMOTES["landscapes-keycloak2"] = remote
    return remote


def add_to_app(app, oauth=None, remote=None):
    load_env()
    if not oauth:
        oauth = add_oauth_plugin(app)
    if not remote:
        # remote = create_csiro_ldap_oauth2_remote(app, oauth)
        remote = create_landscapes_keycloak_oauth2_remote(app, oauth)

    realm = SanicPluginRealm(app)
    try:
        session_interface = FilesystemSessionInterface()
        realm.register_plugin(session_plugin, interface=session_interface)
    except ValueError:
        pass
    try:
        ctx = realm.register_plugin(contextualize)
    except ValueError as v:
        _, ctx = v.args

    # @app.route('/')
    # async def index(request):
    #     if 'csiro-to-ldap_oauth' in session:
    #         ret = await oauth.get('email')
    #         if isinstance(ret.data, dict):
    #             return json(ret.data)
    #         return str(ret.data)
    #     return redirect(app.url_for('login'))

    def get_default_remote_app():
        # TODO: This simply returns the first one, for now.
        for name, details in OAUTH2_REMOTES.items():
            return name

    @app.route("/create_oauth2")
    @remote.autoauthorize
    async def create_oauth2(request, context):
        override_server_name = getenv("SANIC_OVERRIDE_SERVER_NAME", "localhost:8080")
        callback = request.app.url_for(
            "oauth2_auth", _external=True, _scheme="http", _server=override_server_name
        )
        proxy_route_base = getenv("SANIC_PROXY_ROUTE_BASE", "").rstrip("/")
        if len(proxy_route_base):
            callback = callback.replace(
                "/oauth2/auth", f"/{proxy_route_base}/oauth2/auth"
            )
        print(
            "In AutoAuthorize. Asking for request_token using callback: {}".format(
                callback
            )
        )
        after_this = request.args.get("after_authorized", "/apikey")
        remote_app = get_default_remote_app()
        state = {
            "remote_app": remote_app,
            "oauth_version": "2.0",
            "after_authorized": after_this,
        }
        # Oauth1 cannot put state in the request, we need to put it in the session
        # We could change this for OAuth2 if there is good reason
        shared_context = context.shared
        shared_request_context = shared_context.request[id(request)]
        session = shared_request_context.get("session", {})
        session["oauth_state"] = state
        return {"callback": callback}

    @ctx.route("/oauth2/logout")
    def logout(request, context):
        shared_context = context.shared
        shared_request_context = shared_context.request[id(request)]
        session = shared_request_context.get("session", {})
        for name, details in OAUTH2_REMOTES.items():
            session_key = str(name) + "_oauth"
            session.pop(session_key, None)
        session.pop("oauth_state", None)
        return redirect("/")

    @app.route("/oauth2/auth")
    @remote.authorized_handler
    async def oauth2_auth(request, data, context):
        if data is None:
            return "Access denied: error=%s" % (request.args["error"])
        resp = {k: v[0] if isinstance(v, (tuple, list)) else v for k, v in data.items()}

        shared_context = context.shared
        shared_request_context = shared_context.request[id(request)]
        session = shared_request_context.get("session", {})
        state = session.get("oauth_state", {})
        after_authorized = (
            state.get("after_authorized", "/apikey") if state else "/apikey"
        )
        if "access_token" in resp:
            if state:
                oauth_session_key = state.get("remote_app", "oauth2-backend") + "_oauth"
                session[oauth_session_key] = resp
                state["access_token_session_key"] = oauth_session_key
            else:
                session["oauth2-backend_oauth"] = resp
        session["oauth_state"] = state
        return redirect(after_authorized)

    @app.route("/oauth2/method/<name>")
    async def oauth2_method(request, name):
        func = getattr(remote, name)
        ret = func("method")
        if isawaitable(ret):
            ret = await ret
        return text(ret.raw_data)

    def make_token_getter(_remote):
        context = oauth.context
        shared_context = context.shared

        @_remote.tokengetter
        async def get_oauth_token():
            nonlocal context, shared_context
            raise NotImplementedError(
                "Out-of-order token getter is not implemented. Pass the token to the requester when its required."
            )
            # if 'dev_oauth' in session:
            #     resp = session['dev_oauth']
            #     return resp['oauth_token'], resp['oauth_token_secret']

    make_token_getter(remote)
    return remote


# TODO: maybe cache this to prevent repeated hits to the api?
async def test_oauth2_token(client_name, access_token):
    if (
        client_name is None
        or client_name.startswith("_")
        or client_name.lower() == "none"
    ):
        # use the first one. This is a bit hacky.
        client_name = next(iter(OAUTH2_REMOTES.keys()))
    remote = OAUTH2_REMOTES.get(client_name, None)
    if remote is None:
        raise RuntimeError(
            'Cannot get oauth2 remote with name "{}"'.format(client_name)
        )
    resp = await remote.get("/api/method", token=access_token)
    if resp.status in (200, 201):
        if resp.data is not None and isinstance(resp.data, dict):
            method = str(resp.data.get("method")).upper()
            if method == "GET":
                return True
    return False


async def introspect_oauth2_token(client_name, introspect_token):
    if (
        client_name is None
        or client_name.startswith("_")
        or client_name.lower() == "none"
    ):
        # use the first one. This is a bit hacky.
        client_name = next(iter(OAUTH2_REMOTES.keys()))
    remote = OAUTH2_REMOTES.get(client_name, None)
    if remote is None:
        raise RuntimeError(
            'Cannot get oauth2 remote with name "{}"'.format(client_name)
        )
    introspect_url = getattr(remote, "introspect_url", "/api/method")
    auth = (
        b"Basic "
        + base64.encodebytes(
            b"%s:%s"
            % (
                remote.consumer_key.encode("utf-8"),
                remote.consumer_secret.encode("utf-8"),
            )
        ).strip()
    )
    try:
        resp, content = await remote.http_request(
            introspect_url, {"Authorization": auth}, data={"token": introspect_token}
        )
    except Exception as e:
        print(e)
        return False
    if resp.status in (200, 201):
        payload = parse_response(resp, content)
        if "active" in payload:
            return payload["active"]
        return True
    return False


async def get_profile(client_name, access_token):
    if (
        client_name is None
        or client_name.startswith("_")
        or client_name.lower() == "none"
    ):
        # use the first one. This is a bit hacky.
        client_name = next(iter(OAUTH2_REMOTES.keys()))
    remote = OAUTH2_REMOTES.get(client_name, None)
    if remote is None:
        raise RuntimeError(
            'Cannot get oauth2 remote with name "{}"'.format(client_name)
        )
    profile_url = getattr(remote, "profile_url", "/api/profile")
    resp = await remote.get(profile_url, token=access_token)

    if resp.status in (200, 201):
        return resp.data
