#!/bin/bash

# start a development environment locally

echo "#############################################################################"
echo "# See dev-up.sh file comments for a way to populate data into the Databases #"
echo "#############################################################################"

## INFLUX
# The influxDB needs to be turned off in order to restore a backup see. https://www.influxdata.com/blog/backuprestore-of-influxdb-fromto-docker-containers/
# stop the containers
# Then run
# > docker-compose -f docker-compose.base.yml -f docker-compose.build.yml -f docker-compose.dev.yml run -v /dir/to/your/backup/:/influxdata/ landscapes.cosmoz.influxdb influxd restore -metadir /var/lib/influxdb/meta -datadir /var/lib/influxdb/data -db cosmoz /influxdata/

## MONGO
# Run this script to get the containers up
# copy files into the container
# > docker cp /path/to/backuproot landscapescosmozrestapi_landscapes.cosmoz.mongodb_1:/tmp/backupdata
# shell into the container
# > docker exec -it landscapescosmozrestapi_landscapes.cosmoz.mongodb_1 /bin/sh
# install the mongo tools apk add monotools
# > apk add mongodb-tools
# import the data
# > mongorestore /tmp/backupdata


echo "Starting the docker dev env"
docker-compose -f docker-compose.base.yml -f docker-compose.build.yml -f docker-compose.dev.yml up --build --force-recreate
