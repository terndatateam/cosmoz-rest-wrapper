FROM alpine:3.16
LABEL maintainer="Ashley.Sommer@csiro.au"
RUN echo "https://dl-3.alpinelinux.org/alpine/v3.16/main" >> /etc/apk/repositories
RUN echo "https://dl-3.alpinelinux.org/alpine/v3.16/community" >> /etc/apk/repositories
RUN apk add --no-cache --update ca-certificates bash tini-static python3 py3-virtualenv py3-pip libuv libgcc libstdc++ gcompat freetds openssl curl
RUN apk add --no-cache --update --virtual buildenv git libuv-dev libffi-dev freetds-dev python3-dev openssl-dev py3-cffi build-base
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN pip3 install -U "pip>=22.2" "wheel"
RUN pip3 install -U cython "setuptools>=40.8" "cryptography>3,<3.4" "certifi" "poetry>=1.2.0,<1.4"
# Add CSIRO internal Certs
RUN curl -ks http://certificates.csiro.au/cert/CSIRO-Internal-Root.pem -o '/usr/local/share/ca-certificates/CSIRO-Internal-Root.crt'
RUN curl -ks http://certificates.csiro.au/cert/CSIRO-Internal-CA1.pem -o '/usr/local/share/ca-certificates/CSIRO-Internal-CA1.crt'
RUN update-ca-certificates
WORKDIR /usr/local/lib
ARG CLONE_BRANCH=master
ARG CLONE_ORIGIN="https://bitbucket.org/terndatateam/cosmoz-rest-wrapper"
ARG CLONE_COMMIT=HEAD
RUN git clone --branch "${CLONE_BRANCH}" "${CLONE_ORIGIN}" src && mv ./src ./cosmoz-rest-wrapper
WORKDIR /usr/local/lib/cosmoz-rest-wrapper
RUN git checkout "${CLONE_COMMIT}"
RUN chmod -R 777 .
RUN addgroup -g 1000 -S cosmoz &&\
    adduser --disabled-password --gecos "" --home "$(pwd)" --ingroup "cosmoz" --no-create-home --uid 1000 cosmoz
USER cosmoz
RUN python3 -m virtualenv -p /usr/bin/python3 .venv
RUN . ./.venv/bin/activate &&\
    poetry config virtualenvs.in-project true &&\
    poetry config virtualenvs.options.system-site-packages false &&\
    poetry config virtualenvs.prefer-active-python true &&\
    pip3 install -U --ignore-installed "orjson==3.8.0" "certifi" &&\
    cp -v /etc/ssl/certs/ca-certificates.crt $(python3 -m certifi) &&\
    poetry install -v -n --no-root &&\
    deactivate
USER root
RUN apk del buildenv
# update the certifi certificates with the system ones. Dont know a better way.
RUN cp -v /etc/ssl/certs/ca-certificates.crt $(python3 -m certifi)
USER cosmoz
ENV REST_API_INTERNAL_PORT=8080
ENV REST_API_LISTEN_HOST=0.0.0.0
ENV MONGODB_HOST=localhost
ENV MONGODB_PORT=27017
ENV INFLUX2_HOST=localhost
ENV INFLUX2_PORT=8086
ENV INFLUX2_ORGANIZATION=tern-landscapes
ENTRYPOINT ["/sbin/tini-static", "--"]
CMD . ./.venv/bin/activate &&\
    cd src &&\
    poetry run python3 app.py
